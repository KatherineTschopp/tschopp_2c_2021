/* Copyright 2018,
 * Juan Manuel Reta
 * jmreta@ingenieria.uner.edu.ar
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
 /*! @mainpage Ejercicio_Parcial
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * El programa recibe los angulos medidos por el potenciometro, y toma mediciones cada 0,15,30,45,60,75 grados de la distancia
 * en la que se encuentran determinados objetos por medio del hc_sr4 y los manda por puerto serie.
 * Calcula el objeto que determina un peligro para el robot por medio de las mediciones obteniendo la minima distancia y la manda por
 * puero serie tambien.
 * Dirige la direccion de desplaamiento del motor y la muestra por medio de los leds de la EDU-CIAA
 *
 * \section hardConn Hardware Connection
 *
 * |    hc_sr4		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO1	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	+5V 	 	| 	+5V			|
 * | 	GND 	 	| 	GND			|
 *
 * |   POTENCIOMETRO|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC 	 	| 	CH1		    |
 * | 	+3,3V 	 	| 	+3,3V		|
 * | 	GND 	 	| 	GND			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Katherine Tschopp
 *
 */

#ifndef _BLINKING_H
#define _BLINKING_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

