El programa recibe los angulos medidos por el potenciometro, y toma mediciones cada 0,15,30,45,60,75
 grados de la distancia en la que se encuentran determinados objetos por medio del hc_sr4 
y los manda por puerto serie.
Calcula el objeto que determina un peligro para el robot por medio de las mediciones obteniendo
 la minima distancia y la manda porpuero serie tambien.
Dirige la direccion de desplaamiento del motor y la muestra por medio de los leds de la EDU-CIAA

Conexionado de harward necesario 
 * |    hc_sr4		|   EDU-CIAA  |
 * |:------------------:|:------------|
 * | 	ECHO1	 	| 	T_FIL2|
 * | 	TRIGGER	 	| 	T_FIL3|
 * | 	+5V 	 	| 	+5V   |
 * | 	GND 	 	| 	GND   |
 *
 * |   POTENCIOMETRO    |   EDU-CIAA	|
 * |:------------------:|:--------------|
 * | 	VCC 	 	| 	CH1     |
 * | 	+3,3V 	 	| 	+3,3V	|
 * | 	GND 	 	| 	GND	|