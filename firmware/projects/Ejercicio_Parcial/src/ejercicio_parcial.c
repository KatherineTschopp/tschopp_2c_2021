/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 /*! @mainpage Ejercicio_Parcial
 *
 * \section genDesc General Description
 *
 * This section describes how the program works
 *
 * El programa recibe los angulos medidos por el potenciometro, y toma mediciones cada 0,15,30,45,60,75 grados de la distancia
 * en la que se encuentran determinados objetos por medio del hc_sr4 y los manda por puerto serie.
 * Calcula el objeto que determina un peligro para el robot por medio de las mediciones obteniendo la minima distancia y la manda por
 * puero serie tambien.
 * Dirige la direccion de desplaamiento del motor y la muestra por medio de los leds de la EDU-CIAA
 *
 *
 * \section hardConn Hardware Connection
 *
 * |    hc_sr4		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO1	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	+5V 	 	| 	+5V			|
 * | 	GND 	 	| 	GND			|
 *
 * |   POTENCIOMETRO|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC 	 	| 	CH1		    |
 * | 	+3,3V 	 	| 	+3,3V		|
 * | 	GND 	 	| 	GND			|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Katherine Tschopp \


/*==================[inclusions]=============================================*/
#include "../inc/ejercicio_parcial.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "goniometro.h"
#include "hc_sr4.h"
#include "timer.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/
#define GRADOS_0 0
#define GRADOS_15 15
#define GRADOS_30 30
#define GRADOS_45 45
#define GRADOS_60 60
#define GRADOS_75 75
#define INICIALIZACION_MIN 10000 // Inicializo el minimo para poder obtener el nuevo, 10000 valor arbitrario para que se puedea encontrar
/*==================[internal data definition]===============================*/
 uint8_t channel =  2 ;/* CH1 =2 */
 uint16_t angulos;
 uint8_t activo =0;
 int16_t distancia;
 int16_t grados;
 int16_t grados_anterior;
 int16_t minimo;
/*==================[external data definition]===============================*/
serial_config UART_USB = {SERIAL_PORT_PC,115200, NULL};
timer_config my_timer = {TIMER_A,1000,&medir_angulos};
/*==================[external functions definition]==========================*/

void medir_angulos() {
	angulos=GoniometroReadAngle();
	switch(angulos) {
	case GRADOS_0  :
	   activo =!activo;
	   grados_anterior=grados;
	   grados=GRADOS_0;
	break;
	case GRADOS_15 :
		activo =!activo;
		grados=GRADOS_15;
	break;
	case GRADOS_30 :
		activo =!activo;
		grados=GRADOS_30;
	break;
	case GRADOS_45 :
		 activo=!activo;
		 grados=GRADOS_45;
	break;
	case GRADOS_60:
		 activo=!activo;
		 grados=GRADOS_60;
	break;
	case GRADOS_75 :
		 activo=!activo;
		 grados_anterior=grados;
		 grados=GRADOS_75;
	break;

	}
         }


/*==================[internal data definition]===============================*/
/*==================[internal functions declaration]=========================*/

/*==================[external functions definition]==========================*/
void SisInit(void)
{
	SystemClockInit();
	LedsInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	GoniometroInit(channel);
	TimerInit(&my_timer);
    TimerStart(TIMER_A);
    UartInit(&UART_USB);
}

int main(void)
{
	SisInit();
    while(1){

   if (activo){
   distancia = HcSr04ReadDistanceCentimeters();
   UartSendString(SERIAL_PORT_PC,UartItoa(grados,10));
   UartSendString(SERIAL_PORT_PC,"º objeto a ");
   UartSendString(SERIAL_PORT_PC,UartItoa(distancia,10));
   UartSendString(SERIAL_PORT_PC, "\n\r");
   if(minimo>distancia){
    minimo=distancia;
    posicion=grados;
   }

   if(grados== GRADOS_75 ) & ( grado_anterior==GRADOS_60)
   { // termina una vuelta, por lo tanto muestro el minimo
	 UartSendString(SERIAL_PORT_PC," Obstaculo en ");
	 UartSendString(SERIAL_PORT_PC,UartItoa(posicion, 10));
	 UartSendString(SERIAL_PORT_PC,"º");
	 UartSendString(SERIAL_PORT_PC, "\n\r");
	 // cambio sentido de giro del motor
	 LedOff(LED_RGB_G);
	 LedOn(LED_RGB_R);
	 // Inicializo el minimo para poder obtener el nuevo.
	 minimo=INICIALIZACION_MIN;
   }
   if (grados==GRADOS_0) & (grado_anterior==GRADOS_15) {
	// termina una vuelta, por lo tanto muestro el minimo
	   UartSendString(SERIAL_PORT_PC," Obstaculo en ");
	   UartSendString(SERIAL_PORT_PC,UartItoa(posicion, 10));
	   UartSendString(SERIAL_PORT_PC,"º");
	   UartSendString(SERIAL_PORT_PC, "\n\r");
	   // cambio sentido de giro del motor
	   LedOn(LED_RGB_G);
	   LedOff(LED_RGB_R);
  // Inicializo el minimo para poder obtener el nuevo.
	   minimo=INICIALIZACION_MIN;
   }
   }

   activo=!activo;
    return 0;
}
/*==================[end of file]============================================*/

