/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../../prueba_ultrasonido/inc/prueba_ultrasonido.h"

#include "hc_sr4.h"
#include "gpio.h"
#include "systemclock.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void)
{
	 int16_t distancia_centimetro;
	 int16_t distancia_pulgadas;
	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2 , GPIO_T_FIL3  );

	while (1) {
		 distancia_centimetro =HcSr04ReadDistanceCentimeters() ;

	    distancia_pulgadas  =HcSr04ReadDistanceInches() ;
	}
	return 0;
}

/*==================[end of file]============================================*/

