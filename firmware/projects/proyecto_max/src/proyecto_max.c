/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/proyecto_max.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "delay.h"
#include "max3010x.h"
#include "uart.h"
#include "spo2_algorithm.h"
/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000

/*==================[internal data definition]===============================*/

uint32_t irBuffer[100]; //infrared LED sensor data
uint32_t redBuffer[100];  //red LED sensor data
int32_t bufferLength; //data length
int32_t spo2; //SPO2 value
int8_t validSPO2; //indicator to show if the SPO2 calculation is valid
int32_t heartRate; //heart rate value
int8_t validHeartRate; //indicator to show if the heart rate calculation is valid
/*==================[internal functions declaration]=========================*/

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();
	MAX3010X_begin();
	MAX3010X_setup( 20, 1 , 2, 400, 411, 4096);
	serial_config UART_USB;
	UART_USB.baud_rate = 115200;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = NULL;
	UartInit(&UART_USB);

	uint8_t i;

	bufferLength = 100; //buffer length of 100 stores 4 seconds of samples running at 25sps

	  //read the first 100 samples, and determine the signal range
	  for (i = 0 ; i < bufferLength ; i++)
	  {
	    while (MAX3010X_available() == false) //do we have new data?
	    	MAX3010X_check(); //Check the sensor for new data

	    redBuffer[i] = MAX3010X_getRed();
	    irBuffer[i] = MAX3010X_getIR();
	    MAX3010X_nextSample(); //We're finished with this sample so move to next sample

	    UartSendString(SERIAL_PORT_PC, "red=");
	    UartSendString(SERIAL_PORT_PC, UartItoa(redBuffer[i], 10));
	    UartSendString(SERIAL_PORT_PC, ", ir=");
	    UartSendString(SERIAL_PORT_PC, UartItoa(irBuffer[i], 10));
	    UartSendString(SERIAL_PORT_PC, "\r\n");
	  }

	  //calculate heart rate and SpO2 after first 100 samples (first 4 seconds of samples)
	  maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);

    while(1)
    {
    	//float temperature = MAX3010X_readTemperature();
    	UartSendString(SERIAL_PORT_PC, UartItoa(MAX3010X_getIR(),10));
    	UartSendString(SERIAL_PORT_PC, ",");
    	UartSendString(SERIAL_PORT_PC, UartItoa(MAX3010X_getRed(),10));
    	UartSendString(SERIAL_PORT_PC, "\r\n");

    	 //dumping the first 25 sets of samples in the memory and shift the last 75 sets of samples to the top
    	    for (i = 25; i < 100; i++)
    	    {
    	      redBuffer[i - 25] = redBuffer[i];
    	      irBuffer[i - 25] = irBuffer[i];
    	    }

    	    //take 25 sets of samples before calculating the heart rate.
    	    for ( i = 75; i < 100; i++)
    	    {
    	      while (MAX3010X_available() == false) //do we have new data?
    	    	  MAX3010X_check(); //Check the sensor for new data

    	      //digitalWrite(readLED, !digitalRead(readLED)); //Blink onboard LED with every data read

    	      redBuffer[i] = MAX3010X_getRed();
    	      irBuffer[i] = MAX3010X_getIR();
    	      MAX3010X_nextSample(); //We're finished with this sample so move to next sample

    	      //send samples and calculation result to terminal program through UART
    	      UartSendString(SERIAL_PORT_PC, "red=");
    	      UartSendString(SERIAL_PORT_PC, UartItoa(redBuffer[i], 10));
    	      UartSendString(SERIAL_PORT_PC, ", ir=");
    	      UartSendString(SERIAL_PORT_PC, UartItoa(irBuffer[i], 10));

    	      UartSendString(SERIAL_PORT_PC, ", HR=");
    	      UartSendString(SERIAL_PORT_PC, UartItoa(heartRate, 10));

    	      UartSendString(SERIAL_PORT_PC, ", HRvalid=");
    	      UartSendString(SERIAL_PORT_PC, UartItoa(validHeartRate, 10));

    	      UartSendString(SERIAL_PORT_PC, ", SPO2=");
    	      UartSendString(SERIAL_PORT_PC, UartItoa(spo2, 10));

    	      UartSendString(SERIAL_PORT_PC, ", SPO2Valid=");
    	      UartSendString(SERIAL_PORT_PC, UartItoa(validSPO2, 10));
    	      UartSendString(SERIAL_PORT_PC, "\r\n");
    	    }

    	    //After gathering 25 new samples recalculate HR and SP02
    	    maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);

    	LedOn(LED_1);
    	DelayMs(500);
		LedOff(LED_1);
		DelayMs(500);
	}
    
	return 0;
}

/*==================[end of file]============================================*/

