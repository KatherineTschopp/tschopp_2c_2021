/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 /*! @mainpage Proyecto_2
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * Mide la distancia por ultrasonido y la muestra por un display.
 * Encendiendo distintos led dependiendo de las distancias medidas
 *  0 y 10 cm, enciende el LED_RGB_B (Azul).
 *  10 y 20 cm, enciende el LED_RGB_B (Azul) y LED_1.
 *  20 y 30 cm, enciende el LED_RGB_B (Azul), LED_1 y LED_2
 *   mayor a 30 cm, enciende el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
 *   Activa y detiene la medicion con tecla 1
 *   Con tecla 2 mantiene el resultado.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |    hc_sr4		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO1	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	+5V 	 	| 	+5V			|
 * | 	GND 	 	| 	GND			|
 *
 * |   Display		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	LCD1	 	| 	D1  		|
 * | 	LCD2    	| 	D2  		|
 * | 	LCD3	 	| 	D3  		|
 * | 	LCD4    	| 	D4 		    |
 * | 	GPIO1	 	| 	SEL_0		|
 * | 	GPIO3	 	| 	SEL_1		|
 * | 	GPIO5	 	| 	SEL_2		|
 * | 	+5V 	 	| 	+5V			|
 * | 	GND 	 	| 	GND			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Katherine Tschopp
 *
 */
/*Proyecto: Medidor de distancia por ultrasonido
Mostrar distancia medida utilizando los leds de la siguiente manera:

Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul).
Si la distancia está entre 10 y 20 cm, encender el LED_RGB_B (Azul) y LED_1.
Si la distancia está entre 20 y 30 cm, encender el LED_RGB_B (Azul), LED_1 y LED_2
Si la distancia es mayor a 30 cm, encender el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.

Mostrar el valor de distancia en cm utilizando el display LCD (si dispone de uno).
Usar TEC1 para activar y detener la medición.
Usar TEC2 para mantener el resultado (“HOLD”).  */

/*==================[inclusions]=============================================*/
#include "../inc/proyecto_2.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/



/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{   uint8_t teclas;
    int16_t distancia;
    uint8_t activo =0;
    uint8_t hold=1;
	SystemClockInit();
	ITSE0803Init();
	LedsInit();
	SwitchesInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
    while(1)
    {


     LedOff(LED_RGB_B);
     LedOff(LED_2);
     LedOff(LED_1);
     LedOff(LED_3);

     teclas  = SwitchesRead();

     switch(teclas) {
            case SWITCH_1:
	              activo=!activo;
             break;

            case SWITCH_2:
           	              hold=!hold;
                        break;
     }

     if (activo){

    	 distancia = HcSr04ReadDistanceCentimeters();

    	if(hold)
    	{
    		if(distancia<10) {
      	  LedOn(LED_RGB_B);
           }
       else  {
       if(distancia<20)
      	  {LedOn(LED_RGB_B);
         	  LedOn(LED_1); }
      	  else{
              if(distancia<30) {
            LedOn(LED_2);
  		  LedOn(LED_RGB_B);
  		  LedOn(LED_1);   }
            else {
          	  LedOn(LED_2);
          	  LedOn(LED_RGB_B);
          	  LedOn(LED_1);
          	  LedOn(LED_3);
            }}}
    	ITSE0803DisplayValue(distancia);
    	}
     }
     else {
    	 ITSE0803DisplayValue(0);
          LedOff(LED_RGB_B);
          LedOff(LED_2);
          LedOff(LED_1);
          LedOff(LED_3);
     }
     DelaySec(1);
     distancia=0;

    
}
    return 0;
}

/*==================[end of file]============================================*/

