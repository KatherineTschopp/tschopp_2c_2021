Aplicación: Control de Acceso

Autor: Albano Peñalva

Descripción:

Esta aplicación implementa un control de acceso a un recinto, a través del uso de tarjetas con RFID.

El mismo está embebido en una EDU-CIAA NXP y consta además de un display LCD color de 2.2" ILI9341 (para la comunicación con el usuario) y un dispositivo de comunicación NFC PN532 (para lectura de las tarjetas).

Modo de uso:

Cuando inicia el sistema, la puerta de acceso permanece cerrada (LED Rojo) y se muestra una pantalla de bienvenida a través del display, indicando que se debe acercar la tarjeta RFID.
Al acercarse una tarjeta, el módulo lee el ID de la misma y lo compara con una base de datos interna.
Si el ID de la tarjeta corresponde a una usuario con permiso de acceso, la puerta desbloquea por 4 segundos (LED Verde) y se muestra por pantalla un mensaje de acceso concedido, registrandose además el nombre del usuario y la hora de ingreso.
En caso de que el ID de la tarjeta no se corresponda con un usuario autorizado, la puerta permanece cerrada (LED Rojo) y se muestra por pantalla un mensaje de acceso denegado.

Conecciones de Hardware:

 |   ili9341	|   EDU-CIAA	|
 |:------------:|:--------------|
 | 	SDO/MISO 	|	SPI_MISO	|
 | 	LED		 	| 	3V3			|
 | 	SCK		 	| 	SPI_SCK		|
 | 	SDI/MOSI 	| 	SPI_MOSI	|
 | 	DC/RS	 	| 	GPIO2		|
 | 	RESET	 	| 	GPIO3		|
 | 	CS		 	| 	GPIO0		|
 | 	GND		 	| 	GND			|
 | 	VCC		 	| 	3V3			|
 |:------------:|:--------------| 
 |    pn532		|   EDU-CIAA	|
 |:------------:|:--------------|
 | 	MISO 		|	SPI_MISO	|
 | 	SCK		 	| 	SPI_SCK		|
 | 	MOSI 		| 	SPI_MOSI	|
 | 	CS		 	| 	GPIO1		|
 | 	GND		 	| 	GND			|
 | 	VCC		 	| 	5V			|