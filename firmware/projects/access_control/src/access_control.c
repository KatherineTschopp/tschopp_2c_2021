/*! @mainpage Access Control
 *
 * @author Albano Peñalva
 * @author Juan Ignacio Cerrudo
 *
 * \section genDesc General Description
 *
 * This application implements an access control to a room,
 * through the use of cards with RFID.
 *
 * It is embedded in an EDU-CIAA NXP and includes a 2.2" LCD color display
 * ILI9341 (for communication with user) and a NFC PN532 communication device
 * (for card reading).
 *
 * \htmlonly <style>div.image img[src="system.jpg"]{width:400px;}</style> \endhtmlonly
 * @image html system.jpg "System"
 *
 * When the system starts, the access door remains closed (Red LED)
 * and a welcome screen is shown through the display, indicating that the
 * RFID card should be approached.
 *
 * \htmlonly <style>div.image img[src="main.jpg"]{width:200px;}</style> \endhtmlonly
 * @image html main.jpg "Welcome Screen"
 *
 * When a card is approached, the module reads the card's ID and compares
 * it with an internal database.
 * If the ID of the card corresponds to a user with access permission, the
 * door unlocks for 4 seconds (Green LED) and an access message is displayed
 * on the screen, also registering the user's name and the time of entry.
 *
 * \htmlonly <style>div.image img[src="granted.jpg"]{width:200px;}</style> \endhtmlonly
 * @image html granted.jpg "Access Granted Screen"
 *
 * In case the ID of the card does not correspond to an authorized user,
 * the door remains closed (Red LED) and an access denied message is displayed
 * on the screen.
 *
 * \htmlonly <style>div.image img[src="denied.jpg"]{width:200px;}</style> \endhtmlonly
 * @image html denied.jpg "Access Denied Screen"
 *
 * <a href="https://youtu.be/keasSuHzakA">Operation Example</a>
 *
 * @section hardConn Hardware Connection
 *
 * |   	ILI9341	    |   EDU-CIAA    |
 * |:--------------:|:--------------|
 * | 	SDO/MISO 	|	SPI_MISO	|
 * | 	LED		 	| 	3V3			|
 * | 	SCK		 	| 	SPI_SCK		|
 * | 	SDI/MOSI 	| 	SPI_MOSI	|
 * | 	DC/RS	 	| 	GPIO2		|
 * | 	RESET	 	| 	GPIO3		|
 * | 	CS		 	| 	GPIO0		|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	3V3			|
 *
 * |     PN532      |   EDU-CIAA    |
 * |:--------------:|:--------------|
 * | 	MISO 		|	SPI_MISO	|
 * | 	SCK		 	| 	SPI_SCK		|
 * | 	MOSI 		| 	SPI_MOSI	|
 * | 	CS		 	| 	GPIO1		|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	5V			|
 *
 * @section firmStruct Firmware Structure
 *
 * Devices Drivers:
 * <a href="ili9341_8h.html">ILI9341</a>,
 * <a href="pn532_8h.html">PN352</a>
 *
 * Peripheral Drivers:
 * <a href="spi_8h.html">SPI</a>,
 * <a href="gpio_8h.html">GPIO</a>,
 * <a href="delay_8h.html">Delay</a>,
 * <a href="led_8h.html">LEDs</a>,
 * <a href="sapi__rtc_8h.html">RTC</a>
 *
 * \htmlonly <style>div.image img[src="layer.jpg"]{width:400px;}</style> \endhtmlonly
 * @image html layer.jpg "Firmware Layer Structure"
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 06/12/2018 | Document creation		                         |
 * | 17/12/2018 | Added hardware connection                      |
 *
 */

#include "access_control.h"
#include "ili9341.h"
#include "pn532.h"
#include "fonts.h"
#include "pictures.h"
#include "led.h"
#include "board.h"
#include "delay.h"
#include "sapi.h"
#include <lpc_types.h>

/*****************************************************************************
 * Private macros/types/enumerations/variables definitions
 ****************************************************************************/

#define MARGIN 20			/*!< Distance in pixels from left border to start writing */
#define CENTER 80			/*!< Distance in pixels to center hour */
#define SPI_1 1				/*!< EDU-CIAA SPI port */
#define GPIO_0 0			/*!< EDU-CIAA GPIO0 port */
#define GPIO_1 1			/*!< EDU-CIAA GPIO1 port */
#define GPIO_2 2			/*!< EDU-CIAA GPIO2 port */
#define GPIO_3 3			/*!< EDU-CIAA GPIO3 port */
#define PN532_SCK 1000000	/*!< SPI bitrate to communicate with PN532 */

/*
 * @brief System states
 */
typedef enum
{
	stand_by,		/*!< Waiting for card to be approached */
	granted,		/*!< Card recognized, access granted */
	denied			/*!< Card not recognized, access denied */
} screen_state_t;

/*
 * @brief Users structure
 */
typedef struct
{
	uint32_t id;	/*!< Number of card ID */
	char *name;		/*!< Name of user */
} user_t;

/*
 * @brief List of users with access permission
 */
const user_t lab_proto_users[] =
{
	{4202522256, "Albano Penalva"},
	{801830985, "Juan I. Cerrudo"},
	{266589533, "Diana Vertiz"}
};

/*****************************************************************************
 * Public types/enumerations/variables declarations
 ****************************************************************************/

/*****************************************************************************
 * Private functions definitions
 ****************************************************************************/

/**
 * @brief		Write time on the LCD
 * @param[in]	h Hour
 * @param[in]	m Minutes
 * @return		None
 * @note
 */
void DrawTime(uint8_t h, uint8_t m);

/**
 * @brief		Update the time on the LCD
 * @param[in]	h Hour
 * @param[in]	m Minutes
 * @return		None
 * @note
 */
void UpdateTime(uint8_t h, uint8_t m);

/*****************************************************************************
 * Private functions declarations
 ****************************************************************************/

void DrawTime(uint8_t h, uint8_t m)
{
	char h1 = (h / 10) + '0';	/* tens of hours */
	char h2 = (h % 10) + '0';	/* units of hours */
	char m1 = (m / 10) + '0';	/* tens of minutes */
	char m2 = (m % 10) + '0';	/* units of minutes */
	/* Draw each number */
	ILI9341DrawChar(CENTER, proto_header.height + 25, h1, &font_16x26,
		ILI9341_BLACK, ILI9341_WHITE);
	ILI9341DrawChar(CENTER + font_16x26.FontWidth,
		proto_header.height + 25, h2, &font_16x26, ILI9341_BLACK, ILI9341_WHITE);
	ILI9341DrawChar(CENTER + (font_16x26.FontWidth * 2),
		proto_header.height + 25, ':', &font_16x26, ILI9341_BLACK, ILI9341_WHITE);
	ILI9341DrawChar(CENTER + (font_16x26.FontWidth * 3),
		proto_header.height + 25, m1, &font_16x26, ILI9341_BLACK, ILI9341_WHITE);
	ILI9341DrawChar(CENTER + (font_16x26.FontWidth * 4),
		proto_header.height + 25, m2, &font_16x26, ILI9341_BLACK, ILI9341_WHITE);
}

void UpdateTime(uint8_t h, uint8_t m)
{
	/* Variables to store the last hour drew */
	static uint8_t hour1 = 0;
	static uint8_t hour2 = 0;
	static uint8_t min1 = 0;
	static uint8_t min2 = 0;
	static uint8_t dot = TRUE;
	/* Decide if number must be updated */
	if (h / 10 != hour1)
	{
		hour1 = h / 10;
		char h1 = (h / 10) + '0';
		ILI9341DrawChar(CENTER, proto_header.height + 25, h1, &font_16x26,
			ILI9341_BLACK, ILI9341_WHITE);
	}
	/* Decide if number must be updated */
	if (h % 10 != hour2)
	{
		hour2 = h % 10;
		char h2 = (h % 10) + '0';
		ILI9341DrawChar(CENTER + font_16x26.FontWidth,
			proto_header.height + 25, h2, &font_16x26, ILI9341_BLACK, ILI9341_WHITE);
	}
	/* Decide if number must be updated */
	if (m / 10 != min1)
	{
		min1 = m / 10;
		char m1 = (m / 10) + '0';
		ILI9341DrawChar(CENTER + (font_16x26.FontWidth * 3),
			proto_header.height + 25, m1, &font_16x26, ILI9341_BLACK, ILI9341_WHITE);
	}
	/* Decide if number must be updated */
	if (m % 10 != min2)
	{
		min2 = m % 10;
		char m2 = (m % 10) + '0';
		ILI9341DrawChar(CENTER + (font_16x26.FontWidth * 4),
			proto_header.height + 25, m2, &font_16x26, ILI9341_BLACK, ILI9341_WHITE);
	}
	/* For dots blinking */
	if (dot)
	{
		ILI9341DrawChar(CENTER + (font_16x26.FontWidth * 2),
			proto_header.height + 25, ':', &font_16x26, ILI9341_BLACK, ILI9341_WHITE);
		dot = FALSE;
	}
	else
	{
		ILI9341DrawChar(CENTER + (font_16x26.FontWidth * 2),
			proto_header.height + 25, ' ', &font_16x26, ILI9341_BLACK, ILI9341_WHITE);
		dot = TRUE;
	}
}

/*****************************************************************************
 * Public functions declarations
 ****************************************************************************/

int main(void)
{
	/* The system initializes in stand-by state */
	screen_state_t state = stand_by;
	uint8_t first_time = TRUE;
	/* Date and time structure */
	rtc_t time = {2018, 12, 20, 5, 20, 27, 30};
	uint8_t user;
	/* Hardware initialization */
	Board_Init();
	ILI9341Init(SPI_1, GPIO_0, GPIO_2, GPIO_3);
	ILI9341Rotate(ILI9341_Portrait_2);
	PN532Init(SPI_1, PN532_SCK, GPIO_1);
	PN532SAMConfig();
	Init_Leds();
	/* If not using battery, configure RTC */
	rtcConfig(&time);
	//rtcRead(&time);
	/* Draw header */
	ILI9341DrawPicture(0, 0, proto_header.width, proto_header.height, proto_header.data);

	while(1)
	{
		switch (state)
		{
		case stand_by:
			if (first_time)
			{
				/* Door closed */
				Led_On(RED_LED);
				Led_Off(GREEN_LED);
				/* Clean screen */
				ILI9341DrawFilledRectangle(MARGIN, proto_header.height, ILI9341_WIDTH,
					ILI9341_HEIGHT, ILI9341_WHITE);
				/* Re-draw screen stand by */
				ILI9341DrawPicture(75, 240, nfc.width, nfc.height, nfc.data);
				DrawTime(time.hour, time.min);
				first_time = FALSE;
			}
			/* Lapse of time between card read attempts */
			DelayMs(500);
			rtcRead(&time);
			UpdateTime(time.hour, time.min);
			/* Read card */
			uint32_t id = PN532ReadPassiveTargetID(PN532_MIFARE_ISO14443A);
			if (id != 0)
			{
				/* A card has been read */
				state = denied;
				for (uint8_t i = 0; i < sizeof(lab_proto_users)/sizeof(user_t); i++)
				{
					/* Search ID in users database */
					if(id == lab_proto_users[i].id)
					{
						/* User with access permission */
						state = granted;
						user = i;
					}
				}
			}
			break;

		case granted:
			/* Door open */
			Led_On(GREEN_LED);
			Led_Off(RED_LED);
			/* Clean screen */
			ILI9341DrawFilledRectangle(MARGIN, proto_header.height, ILI9341_WIDTH,
				ILI9341_HEIGHT, ILI9341_WHITE);
			/* Draw access granted symbol */
			ILI9341DrawPicture(60, 190, access_granted.width, access_granted.height,
				access_granted.data);
			/* Draw welcome message */
			ILI9341DrawString(MARGIN, 120, "Bienvenido:", &font_11x18, ILI9341_BLACK,
				ILI9341_WHITE);
			ILI9341DrawString(MARGIN, 140, lab_proto_users[user].name, &font_11x18,
				ILI9341_BLACK, ILI9341_WHITE);
			ILI9341DrawString(MARGIN, 160, "Ingreso: ", &font_11x18, ILI9341_BLACK,
				ILI9341_WHITE);
			ILI9341DrawChar(MARGIN + (font_11x18.FontWidth * 9), 160, (time.hour / 10) + '0',
				&font_11x18, ILI9341_BLACK, ILI9341_WHITE);
			ILI9341DrawChar(MARGIN + (font_11x18.FontWidth * 10), 160, (time.hour % 10) + '0',
				&font_11x18, ILI9341_BLACK, ILI9341_WHITE);
			ILI9341DrawChar(MARGIN + (font_11x18.FontWidth * 11), 160, ':',
				&font_11x18, ILI9341_BLACK, ILI9341_WHITE);
			ILI9341DrawChar(MARGIN + (font_11x18.FontWidth * 12), 160, (time.min / 10) + '0',
				&font_11x18, ILI9341_BLACK, ILI9341_WHITE);
			ILI9341DrawChar(MARGIN + (font_11x18.FontWidth * 13), 160, (time.min % 10) + '0',
				&font_11x18, ILI9341_BLACK, ILI9341_WHITE);
			state = stand_by;
			first_time = TRUE;
			/* Lapse of time to hold welcome message and door open */
			DelaySec(4);
			break;

		case denied:
			/* Clean screen */
			ILI9341DrawFilledRectangle(MARGIN, proto_header.height, ILI9341_WIDTH,
					ILI9341_HEIGHT, ILI9341_WHITE);
			/* Draw access denied symbol */
			ILI9341DrawPicture(60, 190, access_denied.width, access_denied.height,
					access_denied.data);
			ILI9341DrawString(MARGIN, 140, "No identificado", &font_11x18,
				ILI9341_BLACK, ILI9341_WHITE);
			state = stand_by;
			first_time = TRUE;
			/* Lapse of time to hold access denied message */
			DelaySec(4);
			break;
		}
	}
	return 0;
}
