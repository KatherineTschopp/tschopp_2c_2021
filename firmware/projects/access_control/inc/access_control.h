/*! @mainpage Access Control
 *
 * @author Albano Peñalva
 * @author Juan Ignacio Cerrudo
 *
 * \section genDesc General Description
 *
 * This application implements an access control to a room,
 * through the use of cards with RFID.
 *
 * It is embedded in an EDU-CIAA NXP and includes a 2.2" LCD color display
 * ILI9341 (for communication with user) and a NFC PN532 communication device
 * (for card reading).
 *
 * When the system starts, the access door remains closed (Red LED)
 * and a welcome screen is shown through the display, indicating that the
 * RFID card should be approached.
 * When a card is approached, the module reads the card's ID and compares
 * it with an internal database.
 * If the ID of the card corresponds to a user with access permission, the
 * door unlocks for 4 seconds (Green LED) and an access message is displayed
 * on the screen, also registering the user's name and the time of entry.
 * In case the ID of the card does not correspond to an authorized user,
 * the door remains closed (Red LED) and an access denied message is displayed
 * on the screen.
 *
 * \section hardConn Hardware Connection
 *
 * |   	ili9341		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	SDO/MISO 	|	SPI_MISO	|
 * | 	LED		 	| 	3V3			|
 * | 	SCK		 	| 	SPI_SCK		|
 * | 	SDI/MOSI 	| 	SPI_MOSI	|
 * | 	DC/RS	 	| 	GPIO2		|
 * | 	RESET	 	| 	GPIO3		|
 * | 	CS		 	| 	GPIO0		|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	3V3			|
 *
 * |   	 pn532		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	MISO 		|	SPI_MISO	|
 * | 	SCK		 	| 	SPI_SCK		|
 * | 	MOSI 		| 	SPI_MOSI	|
 * | 	CS		 	| 	GPIO1		|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	5V			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 06/12/2018 | Document creation		                         |
 * | 17/12/2018 | Added hardware connection                      |
 *
 */

#ifndef MIPROYECTO_H
#define MIPROYECTO_H


#include "stdint.h"
#include "board.h"

#define lpc4337            1
#define mk60fx512vlq15     2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/
#if (CPU == mk60fx512vlq15)
/* Reset_Handler is defined in startup_MK60F15.S_CPP */
void Reset_Handler( void );

extern uint32_t __StackTop;
#elif (CPU == lpc4337)
/** \brief Reset ISR
 **
 ** ResetISR is defined in cr_startup_lpc43xx.c
 **
 ** \remark the definition is in
 **         externals/drivers/cortexM4/lpc43xx/src/cr_startup_lpc43xx.c
 **/
extern void ResetISR(void);

/** \brief Stack Top address
 **
 ** External declaration for the pointer to the stack top from the Linker Script
 **
 ** \remark only a declaration is needed, there is no definition, the address
 **         is set in the linker script:
 **         externals/base/cortexM4/lpc43xx/linker/ciaa_lpc4337.ld.
 **/
extern void _vStackTop(void);



void RIT_IRQHandler(void);


#else
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/
#endif /* #ifndef MIPROYECTO_H */

