/* @brief  Pictures to use whith LCD ili9341
 * @author Albano Peñalva
 *
 * @note: Pictures converted to array with http://www.digole.com/tools/PicturetoC_Hex_converter.php
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 03/12/2018 | Document creation		                         |
 *
 */

#ifndef PICTURES_H_
#define PICTURES_H_

#include <stdint.h>

/*****************************************************************************
 * Public macros/types/enumerations/variables definitions
 ****************************************************************************/

/**
 * @brief  Font structure
 */
typedef struct
{
	uint16_t width;    		/*!< Picture width in pixels */
	uint16_t height;   		/*!< Picture height in pixels */
	const uint8_t *data; 	/*!< Pointer to picture data array */
} picture_t;

/**
 * @brief  "Laboratorio de Prototipado" logo
 */
const picture_t proto_header;

/**
 * @brief  Contactless card symbol
 */
const picture_t nfc;

/**
 * @brief  Access granted symbol
 */
const picture_t access_granted;

/**
 * @brief  Access denied symbol
 */
const picture_t access_denied;

#endif /* PICTURES_H_ */
