var annotated_dup =
[
    [ "digitalIO_t", "structdigital_i_o__t.html", "structdigital_i_o__t" ],
    [ "Font_t", "struct_font__t.html", "struct_font__t" ],
    [ "gpioConf_t", "structgpio_conf__t.html", "structgpio_conf__t" ],
    [ "lcd_cmd_t", "structlcd__cmd__t.html", "structlcd__cmd__t" ],
    [ "orientation_properties_t", "structorientation__properties__t.html", "structorientation__properties__t" ],
    [ "rtc_t", "structrtc__t.html", "structrtc__t" ],
    [ "spiConfig_t", "structspi_config__t.html", "structspi_config__t" ],
    [ "user_t", "structuser__t.html", "structuser__t" ]
];