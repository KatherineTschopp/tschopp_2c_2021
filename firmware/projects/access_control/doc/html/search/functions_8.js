var searchData=
[
  ['setchipselect',['SetChipSelect',['../ili9341_8c.html#aeabe935f1652950b6c43f10fe834c7bd',1,'ili9341.c']]],
  ['setcursorposition',['SetCursorPosition',['../ili9341_8c.html#a3ced3a9e7189797ae4b2c5cc34cc1886',1,'ili9341.c']]],
  ['spideinit',['SpiDeInit',['../spi_8c.html#a8e3e05c565b5a37e9b1d39c32da1439f',1,'SpiDeInit(spiPort_t port):&#160;spi.c'],['../spi_8h.html#a8e3e05c565b5a37e9b1d39c32da1439f',1,'SpiDeInit(spiPort_t port):&#160;spi.c']]],
  ['spifree',['SpiFree',['../spi_8c.html#a6d85941058e2f2a394511a0c08ef616b',1,'SpiFree(spiPort_t port):&#160;spi.c'],['../spi_8h.html#a6d85941058e2f2a394511a0c08ef616b',1,'SpiFree(spiPort_t port):&#160;spi.c']]],
  ['spiinit',['SpiInit',['../spi_8c.html#ab8c13a643d448d494cec07376efe1757',1,'SpiInit(spiConfig_t spi):&#160;spi.c'],['../spi_8h.html#ab8c13a643d448d494cec07376efe1757',1,'SpiInit(spiConfig_t spi):&#160;spi.c']]],
  ['spiread',['SpiRead',['../spi_8c.html#ab8e7385796c3533e8b3b1cbbac69aa17',1,'SpiRead(spiPort_t port, uint8_t *rx_buffer, uint32_t rx_buffer_size):&#160;spi.c'],['../spi_8h.html#ab8e7385796c3533e8b3b1cbbac69aa17',1,'SpiRead(spiPort_t port, uint8_t *rx_buffer, uint32_t rx_buffer_size):&#160;spi.c']]],
  ['spireadwrite',['SpiReadWrite',['../spi_8c.html#a48acd58cb71f265b3d1141f319a84a98',1,'SpiReadWrite(spiPort_t port, uint8_t *tx_buffer, uint8_t *rx_buffer, uint32_t buffer_size):&#160;spi.c'],['../spi_8h.html#a48acd58cb71f265b3d1141f319a84a98',1,'SpiReadWrite(spiPort_t port, uint8_t *tx_buffer, uint8_t *rx_buffer, uint32_t buffer_size):&#160;spi.c']]],
  ['spiwrite',['SpiWrite',['../spi_8c.html#a47da8f0dafba321f5414cb8cdfb064d9',1,'SpiWrite(spiPort_t port, uint8_t *tx_buffer, uint32_t tx_buffer_size):&#160;spi.c'],['../spi_8h.html#a47da8f0dafba321f5414cb8cdfb064d9',1,'SpiWrite(spiPort_t port, uint8_t *tx_buffer, uint32_t tx_buffer_size):&#160;spi.c']]],
  ['ssp1_5firqhandler',['SSP1_IRQHandler',['../spi_8c.html#a048b2396e2594865c6947b71774e2328',1,'spi.c']]]
];
