var searchData=
[
  ['red_5fled',['RED_LED',['../led_8h.html#a1f3289eeddfbcff1515a3786dc0518faaedcf3ad9edba345343e3d99b9d27d5a9',1,'led.h']]],
  ['res',['res',['../structgpio_conf__t.html#a6b673623ee9c14d8c7ebbd781c82b4f6',1,'gpioConf_t']]],
  ['reset',['RESET',['../ili9341_8c.html#ab702106cf3b3e96750b6845ded4e0299',1,'ili9341.c']]],
  ['reversebits',['ReverseBits',['../pn532_8c.html#ae5b80987323cbd6271a8a382429cc84a',1,'pn532.c']]],
  ['rgb_5fb_5fled',['RGB_B_LED',['../led_8h.html#a1f3289eeddfbcff1515a3786dc0518faaa7c94ad61c92a8fea11db9b9a4391940',1,'led.h']]],
  ['rgb_5fg_5fled',['RGB_G_LED',['../led_8h.html#a1f3289eeddfbcff1515a3786dc0518faa50aa0cc525d6fa968fa5fb5bf700cdd4',1,'led.h']]],
  ['rgb_5finterface',['RGB_INTERFACE',['../ili9341_8c.html#a798785f739c085efedff5ff9668f70c2',1,'ili9341.c']]],
  ['rgb_5fr_5fled',['RGB_R_LED',['../led_8h.html#a1f3289eeddfbcff1515a3786dc0518faae497ffcd7c6d3b74056ead0449d92f55',1,'led.h']]],
  ['right',['RIGHT',['../ili9341_8c.html#a80fb826a684cf3f0d306b22aa100ddac',1,'ili9341.c']]],
  ['rst_5fint_5fall',['RST_INT_ALL',['../delay_8c.html#a10c31b81b7770fd31fb3b4cab65d1b7b',1,'delay.c']]],
  ['rtc_5ft',['rtc_t',['../structrtc__t.html',1,'']]],
  ['rtcconfig',['rtcConfig',['../sapi__rtc_8c.html#aae022b681586fc7c52ac5e22fb21e77f',1,'rtcConfig(rtc_t *rtc):&#160;sapi_rtc.c'],['../sapi__rtc_8h.html#aae022b681586fc7c52ac5e22fb21e77f',1,'rtcConfig(rtc_t *rtc):&#160;sapi_rtc.c']]],
  ['rtcread',['rtcRead',['../sapi__rtc_8c.html#a7b6e6f1d74053531d6ddba88a7459579',1,'rtcRead(rtc_t *rtc):&#160;sapi_rtc.c'],['../sapi__rtc_8h.html#a7b6e6f1d74053531d6ddba88a7459579',1,'rtcRead(rtc_t *rtc):&#160;sapi_rtc.c']]],
  ['rtcwrite',['rtcWrite',['../sapi__rtc_8c.html#a6328df3e073e0da8a3c0c918a288c17c',1,'rtcWrite(rtc_t *rtc):&#160;sapi_rtc.c'],['../sapi__rtc_8h.html#a6328df3e073e0da8a3c0c918a288c17c',1,'rtcWrite(rtc_t *rtc):&#160;sapi_rtc.c']]]
];
