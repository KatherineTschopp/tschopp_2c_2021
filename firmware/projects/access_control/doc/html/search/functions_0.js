var searchData=
[
  ['delayms',['DelayMs',['../delay_8c.html#a57655b974339443ccc09a5579b8dbeb8',1,'DelayMs(uint32_t msec):&#160;delay.c'],['../delay_8h.html#a57655b974339443ccc09a5579b8dbeb8',1,'DelayMs(uint32_t msec):&#160;delay.c']]],
  ['delaysec',['DelaySec',['../delay_8c.html#a07471e33d1ced89d722035841f872545',1,'DelaySec(uint32_t sec):&#160;delay.c'],['../delay_8h.html#a07471e33d1ced89d722035841f872545',1,'DelaySec(uint32_t sec):&#160;delay.c']]],
  ['delayus',['DelayUs',['../delay_8c.html#aaa99df3e6eae48f285374e576d9bb345',1,'DelayUs(uint32_t usec):&#160;delay.c'],['../delay_8h.html#aaa99df3e6eae48f285374e576d9bb345',1,'DelayUs(uint32_t usec):&#160;delay.c']]],
  ['dma_5firqhandler',['DMA_IRQHandler',['../spi_8c.html#ac1fca8b8a3ce0431d9aebbf432eda751',1,'spi.c']]],
  ['drawtime',['DrawTime',['../access__control_8c.html#ab3465775eef44ac748ad5cf03db9fc08',1,'access_control.c']]]
];
