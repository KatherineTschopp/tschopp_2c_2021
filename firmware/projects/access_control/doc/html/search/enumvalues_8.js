var searchData=
[
  ['slave',['SLAVE',['../spi_8h.html#a427aaea34028469b33fa40e79ddf6813acb616235f3e84323e870be70b278ac7f',1,'spi.h']]],
  ['spi_5f0',['SPI_0',['../spi_8h.html#a025a15bb685586a5fa18facecd473478a92559105229aa7c103fa22311c4343da',1,'spi.h']]],
  ['spi_5f1',['SPI_1',['../spi_8h.html#a025a15bb685586a5fa18facecd473478aa4b24f86e59b845990862af6481c321f',1,'spi.h']]],
  ['spi_5fdma',['SPI_DMA',['../spi_8h.html#a8eca2297218426636952c631c9a8c881abd949082011eb97260fd92af9ffec55a',1,'spi.h']]],
  ['spi_5finterrupt',['SPI_INTERRUPT',['../spi_8h.html#a8eca2297218426636952c631c9a8c881a70e1201d8d9010717b07818bf975c5ed',1,'spi.h']]],
  ['spi_5fpolling',['SPI_POLLING',['../spi_8h.html#a8eca2297218426636952c631c9a8c881a0cb4f6c37501162dcdcd5686e2efea9a',1,'spi.h']]],
  ['stand_5fby',['stand_by',['../access__control_8c.html#ac99fecebca9e3a20e926681fe9e5beb9a33b0670969329c01ad1bb819c6c9c32a',1,'access_control.c']]]
];
