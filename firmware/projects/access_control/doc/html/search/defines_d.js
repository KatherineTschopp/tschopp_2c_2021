var searchData=
[
  ['page_5faddr_5fset',['PAGE_ADDR_SET',['../ili9341_8c.html#a7fa17eb97440c73b81ca87407c164aee',1,'ili9341.c']]],
  ['pin_5fmiso1',['PIN_MISO1',['../spi_8c.html#abdca583a8d933ff14102c86c9b4ade9e',1,'spi.c']]],
  ['pin_5fmosi1',['PIN_MOSI1',['../spi_8c.html#ac18998ada9e5b2df56ff411c5891f3dc',1,'spi.c']]],
  ['pin_5fsck1',['PIN_SCK1',['../spi_8c.html#afd05c5f0e0b7221c6a51b296a657e3f7',1,'spi.c']]],
  ['pixel_5fformat_5fset',['PIXEL_FORMAT_SET',['../ili9341_8c.html#a47ba113dfe8e8118575b3dcbedd9b3ec',1,'ili9341.c']]],
  ['pn532_5fauth_5fwith_5fkeya',['PN532_AUTH_WITH_KEYA',['../pn532_8h.html#a6ae57648b11a8b362f626e948aafb6af',1,'pn532.h']]],
  ['pn532_5fauth_5fwith_5fkeyb',['PN532_AUTH_WITH_KEYB',['../pn532_8h.html#acb3fde55bb5a799afeb067bbda775c7b',1,'pn532.h']]],
  ['pn532_5fbaudrate_5f201k',['PN532_BAUDRATE_201K',['../pn532_8h.html#af5e96cd6b8f55aee61279ec5291c4d67',1,'pn532.h']]],
  ['pn532_5fbaudrate_5f424k',['PN532_BAUDRATE_424K',['../pn532_8h.html#aa108dfab2df5321b1d74291365d0e704',1,'pn532.h']]],
  ['pn532_5ffirmwareversion',['PN532_FIRMWAREVERSION',['../pn532_8h.html#afea73fb4e983ad363ad4e3c17f338499',1,'pn532.h']]],
  ['pn532_5fgetgeneralstatus',['PN532_GETGENERALSTATUS',['../pn532_8h.html#aeaf3ab4a7ea9d913e49f6e93490a5c43',1,'pn532.h']]],
  ['pn532_5fhosttopn532',['PN532_HOSTTOPN532',['../pn532_8h.html#a87819059f0fad113e80ac39f0f3fd46e',1,'pn532.h']]],
  ['pn532_5findataexchange',['PN532_INDATAEXCHANGE',['../pn532_8h.html#a4b689f9d80fc479a8416932cc492f85c',1,'pn532.h']]],
  ['pn532_5finjumpfordep',['PN532_INJUMPFORDEP',['../pn532_8h.html#a2db33f6750f4982eb79dad4948fb3163',1,'pn532.h']]],
  ['pn532_5finlistpassivetarget',['PN532_INLISTPASSIVETARGET',['../pn532_8h.html#a29b8ffd50e32388c10056e33a914d7f2',1,'pn532.h']]],
  ['pn532_5fmifare_5fiso14443a',['PN532_MIFARE_ISO14443A',['../pn532_8h.html#ac51010583b159d775c4c452cf5e16f96',1,'pn532.h']]],
  ['pn532_5fmifare_5fread',['PN532_MIFARE_READ',['../pn532_8h.html#a479f1567f57ebfb894cb0367c9c689e0',1,'pn532.h']]],
  ['pn532_5fmifare_5fwrite',['PN532_MIFARE_WRITE',['../pn532_8h.html#a49f62f64c54e40efed7e1694d6034342',1,'pn532.h']]],
  ['pn532_5fpack_5fbuff_5fsize',['PN532_PACK_BUFF_SIZE',['../pn532_8c.html#a75fba74f0c368cf52e956248242564c9',1,'pn532.c']]],
  ['pn532_5fpostamble',['PN532_POSTAMBLE',['../pn532_8h.html#a3af393b22be8a8b93443acb3e510630a',1,'pn532.h']]],
  ['pn532_5fpreamble',['PN532_PREAMBLE',['../pn532_8h.html#afcb6bea4838f481313c15f234b8faf82',1,'pn532.h']]],
  ['pn532_5fsamconfiguration',['PN532_SAMCONFIGURATION',['../pn532_8h.html#ae9b064f77246e1399377dae82bfcbd1a',1,'pn532.h']]],
  ['pn532_5fsck',['PN532_SCK',['../access__control_8c.html#a19c0d6db90035329695896e259b0da38',1,'access_control.c']]],
  ['pn532_5fspi_5fdataread',['PN532_SPI_DATAREAD',['../pn532_8h.html#ac9384513045d24d258d595b6295542b7',1,'pn532.h']]],
  ['pn532_5fspi_5fdatawrite',['PN532_SPI_DATAWRITE',['../pn532_8h.html#aed59c66a641298424472a3516942ee0d',1,'pn532.h']]],
  ['pn532_5fspi_5fready',['PN532_SPI_READY',['../pn532_8h.html#ad722f542c2d6d62f27ef7700f52c4c38',1,'pn532.h']]],
  ['pn532_5fspi_5fstatread',['PN532_SPI_STATREAD',['../pn532_8h.html#a48de65c533155be0970021cde1087ea9',1,'pn532.h']]],
  ['pn532_5fstartcode1',['PN532_STARTCODE1',['../pn532_8h.html#a9c46166b35d86d0582109f40a16da339',1,'pn532.h']]],
  ['pn532_5fstartcode2',['PN532_STARTCODE2',['../pn532_8h.html#a165cf1ea82c72ba86883f305090c4e8f',1,'pn532.h']]],
  ['pn532_5ftggetdata',['PN532_TGGETDATA',['../pn532_8h.html#a402cfc99ae7b5bf2f799f10087487e0b',1,'pn532.h']]],
  ['pn532_5ftginitastarget',['PN532_TGINITASTARGET',['../pn532_8h.html#a12db75c5e855b8a5c7c3a98c28f7a20e',1,'pn532.h']]],
  ['pn532_5ftgsetdata',['PN532_TGSETDATA',['../pn532_8h.html#a81dc78a6c206ddf0057684ee12237648',1,'pn532.h']]],
  ['pn532_5fwakeup',['PN532_WAKEUP',['../pn532_8h.html#ae5beab84bd44bf2619b87a0a7a61c0d2',1,'pn532.h']]],
  ['port_5fmiso1',['PORT_MISO1',['../spi_8c.html#a4e01e480acbe18712f1f04872274f1d0',1,'spi.c']]],
  ['port_5fmosi1',['PORT_MOSI1',['../spi_8c.html#a33c77f2ef81471566b5182ea5ae1af08',1,'spi.c']]],
  ['port_5fsck1',['PORT_SCK1',['../spi_8c.html#acf40fb602fef2a392a65dcc6e01b8937',1,'spi.c']]],
  ['pos_5fgamma',['POS_GAMMA',['../ili9341_8c.html#a964889b3527f09608fde445c20847b87',1,'ili9341.c']]],
  ['pr_5fzero',['PR_ZERO',['../delay_8c.html#a3b543f16b4a382f92c47a7bf9865dd41',1,'delay.c']]],
  ['pump_5fratio_5fctrl',['PUMP_RATIO_CTRL',['../ili9341_8c.html#adc200d26bdb86547d00275ba038936bf',1,'ili9341.c']]],
  ['pwr_5fctrl1',['PWR_CTRL1',['../ili9341_8c.html#a7a8f9ebe145ba4ff3c8787208f244909',1,'ili9341.c']]],
  ['pwr_5fctrl2',['PWR_CTRL2',['../ili9341_8c.html#adb301f832861ef36b7a9a48d8c77ced9',1,'ili9341.c']]],
  ['pwr_5fctrl_5fa',['PWR_CTRL_A',['../ili9341_8c.html#adb7ea53332699577f0e0615520c484d1',1,'ili9341.c']]],
  ['pwr_5fctrl_5fb',['PWR_CTRL_B',['../ili9341_8c.html#a4c878fb5f2335f89220d476a5214325a',1,'ili9341.c']]],
  ['pwr_5fon_5fctrl',['PWR_ON_CTRL',['../ili9341_8c.html#a37fc0ccd6096f0b3961023d6689b84f2',1,'ili9341.c']]]
];
