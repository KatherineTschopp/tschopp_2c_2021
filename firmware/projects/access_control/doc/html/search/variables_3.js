var searchData=
[
  ['data',['data',['../structlcd__cmd__t.html#abe222f6d3581e7920dcad5306cc906a8',1,'lcd_cmd_t::data()'],['../struct_font__t.html#a83a3ec7b6a8a7512fe92002a4663ccc4',1,'Font_t::data()']]],
  ['databytes',['databytes',['../structlcd__cmd__t.html#a7533a896b545f6fcffae3956eb5d3a6f',1,'lcd_cmd_t']]],
  ['dir',['dir',['../structgpio_conf__t.html#a5077436f70d8970a60b85342e715c0a5',1,'gpioConf_t']]],
  ['disp_5ffun_5fctrl',['disp_fun_ctrl',['../ili9341_8c.html#a1c5f9f3b3ce68b179a541aaeb825cd02',1,'ili9341.c']]],
  ['dma_5fch_5fssp1_5frx',['dma_ch_ssp1_rx',['../spi_8c.html#a51cbfecbd1cc6554d1de49c98e80d701',1,'spi.c']]],
  ['dma_5fch_5fssp1_5ftx',['dma_ch_ssp1_tx',['../spi_8c.html#ae50bb868e90f45d45e2fa1559f951617',1,'spi.c']]],
  ['driv_5ftim_5fctrl_5fa',['driv_tim_ctrl_a',['../ili9341_8c.html#afc83c18e8d0854b8b4cdb9aca29c312b',1,'ili9341.c']]],
  ['driv_5ftim_5fctrl_5fb',['driv_tim_ctrl_b',['../ili9341_8c.html#a9f16309f2f64840481039f862dac26e6',1,'ili9341.c']]]
];
