var searchData=
[
  ['sec',['sec',['../structrtc__t.html#ad1696900026b287a87c563b733a21bc3',1,'rtc_t']]],
  ['setcs',['SetCS',['../structspi_config__t.html#a9d951f7e6018eda37ae68a3819c5fbce',1,'spiConfig_t']]],
  ['setcs1',['SetCS1',['../spi_8c.html#ae620370f8dbe9790185be5a181985c2a',1,'spi.c']]],
  ['spi_5fchip_5fselect',['spi_chip_select',['../pn532_8c.html#ad12e3702ce5c221a4a57c63217ea152e',1,'pn532.c']]],
  ['spi_5fconf',['spi_conf',['../ili9341_8c.html#ab6bcd62e69e810fbf184ed63cb90b487',1,'ili9341.c']]],
  ['spi_5fconf_5fpn532',['spi_conf_pn532',['../pn532_8c.html#ae66ca09d0cd6682ce5716fe7255d299e',1,'pn532.c']]],
  ['ssp1_5fdata',['ssp1_data',['../spi_8c.html#aa2541db4f35037757cb19814c092e1f5',1,'spi.c']]],
  ['ssp1_5fdma_5frx_5fcompleted',['ssp1_dma_rx_completed',['../spi_8c.html#a6fd3a2bf0e2e85201e37461c1a849cde',1,'spi.c']]],
  ['ssp1_5fdma_5ftx_5fcompleted',['ssp1_dma_tx_completed',['../spi_8c.html#aba5bffeb4d16c14365a0034e654ba8bb',1,'spi.c']]],
  ['ssp1_5ftransfer_5fcompleted',['ssp1_transfer_completed',['../spi_8c.html#a97c1b666b6cd6fa30facf6ecdbeabefe',1,'spi.c']]],
  ['ssp1_5ftransfer_5fmode',['ssp1_transfer_mode',['../spi_8c.html#a2d1fa9daa6001fabb14b8ed5cb729169',1,'spi.c']]]
];
