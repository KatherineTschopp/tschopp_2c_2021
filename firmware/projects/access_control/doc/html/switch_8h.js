var switch_8h =
[
    [ "lpc4337", "switch_8h.html#adfc13aced9eecd5bf67ab539639ef200", null ],
    [ "mk60fx512vlq15", "switch_8h.html#ac5996bc3bcae001e239c5563704c0d7d", null ],
    [ "SWITCHES", "switch_8h.html#aa87203a5637fb4759a378b579aaebff6", [
      [ "TECLA1", "switch_8h.html#aa87203a5637fb4759a378b579aaebff6a7d2d4d7767c90c023bcf91d65858950b", null ],
      [ "TECLA2", "switch_8h.html#aa87203a5637fb4759a378b579aaebff6a022212c2b30d036df890b08f2f196e05", null ],
      [ "TECLA3", "switch_8h.html#aa87203a5637fb4759a378b579aaebff6abf856f5a128061ca7a000f91c022231e", null ],
      [ "TECLA4", "switch_8h.html#aa87203a5637fb4759a378b579aaebff6a216161338f97a094bec10dbabe5dcbe4", null ]
    ] ],
    [ "Init_Switches", "switch_8h.html#a567c9e752c8f560960a5bacce2f3cc04", null ],
    [ "Read_Switches", "switch_8h.html#a062112e932fd77a1fa64764587b1df98", null ]
];