var access__control_8c =
[
    [ "user_t", "structuser__t.html", "structuser__t" ],
    [ "CENTER", "access__control_8c.html#a824fea1f256659e11e6cd8c82cb13338", null ],
    [ "GPIO_0", "access__control_8c.html#a195032917f4b8f4d11b6df67a20888ca", null ],
    [ "GPIO_1", "access__control_8c.html#a32916a847090b087076a5bec92c78c33", null ],
    [ "GPIO_2", "access__control_8c.html#ae5b14b85fd16cf9cc9c37cecf533f83b", null ],
    [ "GPIO_3", "access__control_8c.html#abac2cd6960fc663d2e603e84947b685a", null ],
    [ "MARGIN", "access__control_8c.html#ab05a798afd72aac947f417e1dab73c87", null ],
    [ "PN532_SCK", "access__control_8c.html#a19c0d6db90035329695896e259b0da38", null ],
    [ "SPI_1", "access__control_8c.html#af8df4296869bcc24ea49cc9d1075c74a", null ],
    [ "screen_state_t", "access__control_8c.html#ac99fecebca9e3a20e926681fe9e5beb9", [
      [ "stand_by", "access__control_8c.html#ac99fecebca9e3a20e926681fe9e5beb9a33b0670969329c01ad1bb819c6c9c32a", null ],
      [ "granted", "access__control_8c.html#ac99fecebca9e3a20e926681fe9e5beb9aaba1863cbd09b843372cf7211c16967b", null ],
      [ "denied", "access__control_8c.html#ac99fecebca9e3a20e926681fe9e5beb9a162d4ab6159f42fba6039caf08854c4b", null ]
    ] ],
    [ "DrawTime", "access__control_8c.html#ab3465775eef44ac748ad5cf03db9fc08", null ],
    [ "main", "access__control_8c.html#a840291bc02cba5474a4cb46a9b9566fe", null ],
    [ "UpdateTime", "access__control_8c.html#a30a7b99025f3dfe489ab367504b3713f", null ],
    [ "lab_proto_users", "access__control_8c.html#a640719a25c622eba9705059823533228", null ]
];