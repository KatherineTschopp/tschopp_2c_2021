var ili9341_8h =
[
    [ "ILI9341_BLACK", "ili9341_8h.html#a50166b9a65012fb2f8fe0a0d5f519fa3", null ],
    [ "ILI9341_BLUE", "ili9341_8h.html#aa15cf651d66764cb8024808d6ea48eef", null ],
    [ "ILI9341_BLUE2", "ili9341_8h.html#a7753f9cc58d6e16ab69548349247922e", null ],
    [ "ILI9341_BROWN", "ili9341_8h.html#a204e287c2484d745d3332e5b1e67ae2f", null ],
    [ "ILI9341_CYAN", "ili9341_8h.html#af04ea4a1f89418c734ba76739a2e730f", null ],
    [ "ILI9341_DARKCYAN", "ili9341_8h.html#adbc78927dc7105782714d69048580576", null ],
    [ "ILI9341_DARKGREEN", "ili9341_8h.html#a5a9d46f3af002ee1edc832257f3683a3", null ],
    [ "ILI9341_DARKGREY", "ili9341_8h.html#a56f69dad2de457c03d71ea0b38395c41", null ],
    [ "ILI9341_GREEN", "ili9341_8h.html#a2eb4b5e896b0b144970f33002134bb98", null ],
    [ "ILI9341_GREEN2", "ili9341_8h.html#ac7917ebbf25b3d806541ee4ed658b6c4", null ],
    [ "ILI9341_GREENYELLOW", "ili9341_8h.html#a5f9867862411a57b73cecbc0155e7507", null ],
    [ "ILI9341_HEIGHT", "ili9341_8h.html#a19b26d4d6ae752db7302c9cbb8688d51", null ],
    [ "ILI9341_LIGHTGREY", "ili9341_8h.html#a35990ea476b079cd6746056777fadfd8", null ],
    [ "ILI9341_MAGENTA", "ili9341_8h.html#ae858232678ff269cf0edd404aabcb989", null ],
    [ "ILI9341_MAROON", "ili9341_8h.html#a8fdcfa7e66294fbedf701b2822a9c6e9", null ],
    [ "ILI9341_NAVY", "ili9341_8h.html#acaae04786d06f0ba08c8bea6b714b572", null ],
    [ "ILI9341_OLIVE", "ili9341_8h.html#ae8f5be0a5e731a047a1a7b5edaa0e46f", null ],
    [ "ILI9341_ORANGE", "ili9341_8h.html#a857910d1b98cb9d77a29c2beb5af949d", null ],
    [ "ILI9341_ORANGE2", "ili9341_8h.html#ae518d9f896bf8456a175a5a04b49c7f3", null ],
    [ "ILI9341_PINK", "ili9341_8h.html#ad39cea2dab75ccfd9d83fab73ce1a56d", null ],
    [ "ILI9341_PIXEL_MAX", "ili9341_8h.html#a77c432b28a66dde602c46e7fcba4cc08", null ],
    [ "ILI9341_PURPLE", "ili9341_8h.html#a4371aa3db522c197cae529a235553071", null ],
    [ "ILI9341_RED", "ili9341_8h.html#a4ebe718620305116033cb3f969cfc08b", null ],
    [ "ILI9341_WHITE", "ili9341_8h.html#a604507ef031b95291b2d177088fe03e4", null ],
    [ "ILI9341_WIDTH", "ili9341_8h.html#a0f07262d5faa91c725fa69a2847303d5", null ],
    [ "ILI9341_YELLOW", "ili9341_8h.html#a0313d113622009bfd9b6011fc9bffcc9", null ],
    [ "ili9341_orientation_t", "ili9341_8h.html#a9633e007ade8daf7cb223c46927920a8", [
      [ "ILI9341_Portrait_1", "ili9341_8h.html#a9633e007ade8daf7cb223c46927920a8a8a91947a781dff5aa13fee4123fc4017", null ],
      [ "ILI9341_Portrait_2", "ili9341_8h.html#a9633e007ade8daf7cb223c46927920a8a5355240517a88f9c791c786f52c25a0b", null ],
      [ "ILI9341_Landscape_1", "ili9341_8h.html#a9633e007ade8daf7cb223c46927920a8a4a306e50be2b7a9f5ff45ed5a6a5ab6d", null ],
      [ "ILI9341_Landscape_2", "ili9341_8h.html#a9633e007ade8daf7cb223c46927920a8a80467f7bb95409c5d6f7d943896d3c5c", null ]
    ] ],
    [ "ILI9341DeInit", "ili9341_8h.html#afae878cd3134badb186d4f6eae0c5de9", null ],
    [ "ILI9341DrawChar", "ili9341_8h.html#af40f745e33f77553ed8756b0cdbff963", null ],
    [ "ILI9341DrawCircle", "ili9341_8h.html#a232a7c83cb705bd8c14d57dee26d0db7", null ],
    [ "ILI9341DrawFilledCircle", "ili9341_8h.html#a12d38b5bae7cf33dce21b97e9ed7f114", null ],
    [ "ILI9341DrawFilledRectangle", "ili9341_8h.html#ae317015706c9711c49b9419cd3d89823", null ],
    [ "ILI9341DrawLine", "ili9341_8h.html#a479dc2df6cfdfa86bf9c5123721f8e49", null ],
    [ "ILI9341DrawPicture", "ili9341_8h.html#aef4d22da8546573df6381d4d0f3911f0", null ],
    [ "ILI9341DrawPixel", "ili9341_8h.html#a73585c8169e7a54bc906df9ba1b82752", null ],
    [ "ILI9341DrawRectangle", "ili9341_8h.html#a90867195c7e57957fe9db4913beeee45", null ],
    [ "ILI9341DrawString", "ili9341_8h.html#a1dd6ea8dd3b21f3c5e4c08a5ed2346aa", null ],
    [ "ILI9341Fill", "ili9341_8h.html#adbfd33ee5866c7e8283e5d29d2843b5f", null ],
    [ "ILI9341GetStringSize", "ili9341_8h.html#a5044d1ea4ba5b9b9244bc13898a514d2", null ],
    [ "ILI9341Init", "ili9341_8h.html#a6f9dd33ff2523538d152d7cb3a2af1f9", null ],
    [ "ILI9341Rotate", "ili9341_8h.html#a7b187b4cb92aa6b28a51fbfd5153aa2b", null ]
];