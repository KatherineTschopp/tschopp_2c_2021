var switch_8c =
[
    [ "INPUT_DIRECTION", "switch_8c.html#ad3841a9afe17e5ace60509ee97ed4ee6", null ],
    [ "OUTPUT_DIRECTION", "switch_8c.html#a4965a96f1ffcae1fd1e0606d0cc8ee11", null ],
    [ "SW1_GPIO_PIN", "switch_8c.html#a7f73d8a79ac572ab5fde40277ffccb9b", null ],
    [ "SW1_GPIO_PORT", "switch_8c.html#a1e2971e9594441062d4dbf4d73170855", null ],
    [ "SW1_MUX_GROUP", "switch_8c.html#ae172ab8c8b1d72db0b56d70e91ed84ff", null ],
    [ "SW1_MUX_PIN", "switch_8c.html#aa38faa20021f1e4beadd3fe1450bacdd", null ],
    [ "SW2_GPIO_PIN", "switch_8c.html#a2f792fe06770de33c9ededb87b54459d", null ],
    [ "SW2_GPIO_PORT", "switch_8c.html#a5fdf742d048b6280c2393fb682550286", null ],
    [ "SW2_MUX_GROUP", "switch_8c.html#a41a73bb84565fca627bf3086a62a1251", null ],
    [ "SW2_MUX_PIN", "switch_8c.html#a50de91efe71451c3d8ce3362b27861a6", null ],
    [ "SW3_GPIO_PIN", "switch_8c.html#a34e4eb70962c630fc433bb3b4d133056", null ],
    [ "SW3_GPIO_PORT", "switch_8c.html#afc7dd6b65fcdbd9cef6b9c69bd23dbe8", null ],
    [ "SW3_MUX_GROUP", "switch_8c.html#a3ad1045d911d1fa49246c8825c1d1989", null ],
    [ "SW3_MUX_PIN", "switch_8c.html#ad1eab93224b808749406682836b57a0b", null ],
    [ "SW4_GPIO_PIN", "switch_8c.html#af2ec4c21fd6ea6ef33963f7b0ba28137", null ],
    [ "SW4_GPIO_PORT", "switch_8c.html#a594c4f59c292548143f9a2fae6f68e53", null ],
    [ "SW4_MUX_GROUP", "switch_8c.html#ae6158b4493bd7a00a78dbd7625c140e1", null ],
    [ "SW4_MUX_PIN", "switch_8c.html#a42412d84cfaa7617db79149e81a4b1c5", null ],
    [ "Init_Switches", "switch_8c.html#a567c9e752c8f560960a5bacce2f3cc04", null ],
    [ "Read_Switches", "switch_8c.html#a062112e932fd77a1fa64764587b1df98", null ]
];