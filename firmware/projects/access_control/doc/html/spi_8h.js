var spi_8h =
[
    [ "spiConfig_t", "structspi_config__t.html", "structspi_config__t" ],
    [ "clkMode_t", "spi_8h.html#a2e3236199f0007eb5a75e17be158613b", [
      [ "MODE0", "spi_8h.html#a2e3236199f0007eb5a75e17be158613ba8c818ffe07c247353de39ebce7a017c2", null ],
      [ "MODE1", "spi_8h.html#a2e3236199f0007eb5a75e17be158613bac96709e78c1f5f5e077b68b1c65d141d", null ],
      [ "MODE2", "spi_8h.html#a2e3236199f0007eb5a75e17be158613ba7e24ffb02d34c2b3a679095809eafc55", null ],
      [ "MODE3", "spi_8h.html#a2e3236199f0007eb5a75e17be158613ba7b316658e8633844a318cde3660e5a77", null ]
    ] ],
    [ "spiMode_t", "spi_8h.html#a427aaea34028469b33fa40e79ddf6813", [
      [ "SLAVE", "spi_8h.html#a427aaea34028469b33fa40e79ddf6813acb616235f3e84323e870be70b278ac7f", null ],
      [ "MASTER", "spi_8h.html#a427aaea34028469b33fa40e79ddf6813ae5807df697b52e8b944bf598cabadb3a", null ]
    ] ],
    [ "spiPort_t", "spi_8h.html#a025a15bb685586a5fa18facecd473478", [
      [ "SPI_0", "spi_8h.html#a025a15bb685586a5fa18facecd473478a92559105229aa7c103fa22311c4343da", null ],
      [ "SPI_1", "spi_8h.html#a025a15bb685586a5fa18facecd473478aa4b24f86e59b845990862af6481c321f", null ]
    ] ],
    [ "transferMode_t", "spi_8h.html#a8eca2297218426636952c631c9a8c881", [
      [ "SPI_POLLING", "spi_8h.html#a8eca2297218426636952c631c9a8c881a0cb4f6c37501162dcdcd5686e2efea9a", null ],
      [ "SPI_INTERRUPT", "spi_8h.html#a8eca2297218426636952c631c9a8c881a70e1201d8d9010717b07818bf975c5ed", null ],
      [ "SPI_DMA", "spi_8h.html#a8eca2297218426636952c631c9a8c881abd949082011eb97260fd92af9ffec55a", null ]
    ] ],
    [ "SpiDeInit", "spi_8h.html#a8e3e05c565b5a37e9b1d39c32da1439f", null ],
    [ "SpiFree", "spi_8h.html#a6d85941058e2f2a394511a0c08ef616b", null ],
    [ "SpiInit", "spi_8h.html#ab8c13a643d448d494cec07376efe1757", null ],
    [ "SpiRead", "spi_8h.html#ab8e7385796c3533e8b3b1cbbac69aa17", null ],
    [ "SpiReadWrite", "spi_8h.html#a48acd58cb71f265b3d1141f319a84a98", null ],
    [ "SpiWrite", "spi_8h.html#a47da8f0dafba321f5414cb8cdfb064d9", null ]
];