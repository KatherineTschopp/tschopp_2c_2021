/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 /*! @mainpage Proyecto_final
 *
 * \section genDesc General Description
 *
 * This section describes how the program works
 *
 *Este programa es una aplicacion de un puesto sanitario, en donde al haber una persona se le realiza la medicion de la temperatura
 * de la frecuencia cardiaca, y la oximetria.
 *Estos datos son mandados por puerto serie y por una pantalla.
 * Si el usario presenta una temperatura mayor a 37ºC se enciende el led rojo, si es menor se enciende un led verde.
 * Este programa se enciende y se apaga por la tecla 2
 * Muestra por pantalla una grafica de la oximetria
 *
 * \section hardConn Hardware Connection
 *
 * |    MPC9700		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC 	 	| 	CH1		    |
 * | 	+3,3V 	 	| 	+3,3V		|
 * | 	GND 	 	| 	GND			|
 *
 * |   MA30102		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	IRD 	 	| 	-   		|
 * | 	INT     	| 	-   		|
 * | 	SDA	 	    | 	I2C_SDA     |
 * | 	SCL     	| 	I2C_SCL	    |
 * | 	VIN 	 	| 	+3.3V		|
 * | 	GND 	 	| 	GND			|
 *
 * |   	ILI9341	    |   EDU-CIAA    |
 * |:--------------:|:--------------|
 * | 	SDO/MISO 	|	SPI_MISO	|
 * | 	LED		 	| 	3V3			|
 * | 	SCK		 	| 	SPI_SCK		|
 * | 	SDI/MOSI 	| 	SPI_MOSI	|
 * | 	DC/RS	 	| 	GPIO2		|
 * | 	RESET	 	| 	GPIO3		|
 * | 	CS		 	| 	GPIO0		|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	3V3			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 6/10 /2021 | creation del programa	                         |
 * | 11/11 /2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Katherine Tschopp \


/*==================[inclusions]=============================================*/
#include "../inc/proyecto_final.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "delay.h"
#include "timer.h"
#include "uart.h"
#include "max3010x.h"
#include "spo2_algorithm.h"
#include "MPC9700.h"
#include "ili9341.h"
#include "switch.h"
#include "realtimeplot.h"
#include "iir.h"
#include "highpass.h"

/*==================[macros and definitions]=================================*/
#define FINGER_ON 30000 // si la señal roja es más baja que esto, indica que su dedo no está en el sensor
#define SPI_1 1
#define MARGIN 20			/*!< Distance in pixels from left border to start writing */
#define CENTER 80			/*!< Distance in pixels to center hour */
#define max(a, b) (((a) > (b)) ? (a) : (b))  /*! definiciones para filtro */
#define abs(a) (((a) < (0)) ? (-a) : (a))
#define wL_HP 5

/*==================[internal data definition]===============================*/
 uint8_t channel =  CH1;
 int32_t banner=false;
 float temp;
 int32_t bufferLength = 100; //buffer length of 100 stores 4 seconds of samples running at 25sps
 uint32_t irBuffer[100]; //infrared LED sensor data
 uint32_t redBuffer[100];  //red LED sensor data
 int32_t spo2; //SPO2 value
 int8_t validSPO2; //indicator to show if the SPO2 calculation is valid
 int32_t heartRate; //heart rate value
 int8_t validHeartRate; //indicator to show if the heart rate calculation is valid
 int32_t on_off=false;
 void check (void);
 void ON_OFF (void);
 double w_HP[wL_HP];	// vector de estados internos del filtro pasa altos
 uint16_t filtrada;
/*==================[external data definition]===============================*/
 serial_config UART_USB = {SERIAL_PORT_PC,115200, NULL};
 timer_config my_timer = {TIMER_A,1000,&check};
	signal_t graf = {
				10,
				0,
				ILI9341_YELLOW,
				0,
				0
		};
	 plot_t plot1 = {
	     			10,
	     			120,
	     			290,
	     			105,
	     			100,
	     			ILI9341_BLACK
	     	};
/*==================[external functions definition]==========================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/
 void check (void){
	 int32_t aux =MAX3010X_getRed();
	 if (aux>FINGER_ON){
		 banner=true;
	 }
	 else banner =false;
 }
 void ON_OFF (void){
	 on_off=!on_off;
     if (on_off){
	  ILI9341DrawString(CENTER, 20, "BIENVENIDO", &font_11x18, ILI9341_BLACK, ILI9341_WHITE);
	  DelayMs(1000);
	 ILI9341DrawFilledRectangle(0, 0, 320, 80, ILI9341_WHITE);
	 ILI9341DrawString(CENTER, 20, " ESPERANDO", &font_11x18, ILI9341_BLACK, ILI9341_WHITE);
	 DelayMs(1000);}
     else {
     ILI9341DrawFilledRectangle(0, 0, 320, 80, ILI9341_WHITE);
     ILI9341DrawFilledRectangle(10, 120, 300, 235, ILI9341_BLACK);
     ILI9341DrawString(CENTER, 20, "APAGADO", &font_11x18, ILI9341_BLACK, ILI9341_WHITE);}
     }

/*==================[external functions definition]==========================*/
void SisInit(void)
{
	SystemClockInit();
    MAX3010X_begin();
    MAX3010X_setup( 20, 1 , 2, 400, 411, 4096);
    UartInit(&UART_USB);
    LedsInit();
    MPC9700Init(channel);
    ILI9341Init(SPI_1, GPIO_1, GPIO_5, GPIO_3);
    ILI9341Rotate(ILI9341_Landscape_2);
    SwitchesInit();
    SwitchActivInt(SWITCH_2,  &ON_OFF);
	TimerInit(&my_timer);
    TimerStart(TIMER_A);
	//Variables para graficacion

  	RTPlotInit(&plot1);

    RTSignalInit(&plot1, &graf);

}

int main(void)
{
	SisInit();

    while(1){
    //  Esta prendido el aparato
  if (on_off){
	  // Hay alguien con el dedo apoyado
	 if (banner){
			temp = MPC9700ReadTemp();
			uint8_t i;
			uint8_t j;
		   //read the first 100 samples, and determine the signal rang
			for (i = 0 ; i < bufferLength ; i++) {
			while (MAX3010X_available() == false) //do we have new data?
			MAX3010X_check(); //Check the sensor for new data
			redBuffer[i] = MAX3010X_getRed();
			irBuffer[i] = MAX3010X_getIR();
			MAX3010X_nextSample(); //We're finished with this sample so move to next sample
			// APLICO FILTRO PASA ALTOS
			filtrada = iir(DL_HP - 1, DEN_HP, NL_HP - 1, NUM_HP, w_HP, irBuffer[i]);
		    RTPlotDraw(&graf,filtrada);
			}
		//calculate heart rate and SpO2 after first 100 samples (first 4 seconds of samples)
		  maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);
		  ILI9341DrawFilledRectangle(0, 0, 320, 80, ILI9341_WHITE);
        //  verifico si es valido el valor de oxigeno obtenido
		    if  (validSPO2==1){
			UartSendString(SERIAL_PORT_PC, "El valor de su oximetria es:");
			UartSendString(SERIAL_PORT_PC, UartItoa(spo2, 10));
			UartSendString(SERIAL_PORT_PC, "\n\r");
			ILI9341DrawString(MARGIN, 20, "Oximetria:", &font_11x18, ILI9341_BLACK, ILI9341_WHITE);
			ILI9341DrawString(160, 20,UartItoa(spo2, 10) , &font_11x18,  ILI9341_BLACK, ILI9341_WHITE);  }
			else {
				 UartSendString(SERIAL_PORT_PC, "Oximetria no se pudo obtener ");
				 UartSendString(SERIAL_PORT_PC, "\n\r");
				 ILI9341DrawString(MARGIN,20, "Oximetria no se pudo obtener", &font_11x18, ILI9341_BLACK,
				  ILI9341_WHITE); }
	    //  verifico si es valido el valor de frecuencia cardiaca obtenido
			if  (validHeartRate==1){
			UartSendString(SERIAL_PORT_PC, "Su frecuencia cardiaca es ");
			UartSendString(SERIAL_PORT_PC, UartItoa(heartRate, 10));
			UartSendString(SERIAL_PORT_PC, "\n\r");
			ILI9341DrawString(MARGIN, 40, "Frecuencia cardiaca:", &font_11x18, ILI9341_BLACK, ILI9341_WHITE);
			ILI9341DrawString(260,40,UartItoa(heartRate, 10) , &font_11x18,  ILI9341_BLACK, ILI9341_WHITE);  }
			else {
			   UartSendString(SERIAL_PORT_PC, "Su frecuencia cardiaca no se pudo obtener ");
			   UartSendString(SERIAL_PORT_PC, "\n\r");
			   ILI9341DrawString(MARGIN, 40, "Su frecuencia cardiaca no se pudo obtener", &font_11x18, ILI9341_BLACK,
			   ILI9341_WHITE);}
		//  Muestro el valor de temperatura obtenido
			UartSendString(SERIAL_PORT_PC, "Su temperatura es ");
			UartSendString(SERIAL_PORT_PC, UartItoa(temp, 10));
			UartSendString(SERIAL_PORT_PC, "\n\r");
			ILI9341DrawString(MARGIN, 60, "Temperatura:", &font_11x18, ILI9341_BLACK, ILI9341_WHITE);
			ILI9341DrawString(160, 60,UartItoa(temp, 10) , &font_11x18,  ILI9341_BLACK, ILI9341_WHITE);
		// verifico el valor de temperatura obtenido y enciendo el led correspondiente
			if (temp>37){
				  LedOn(LED_RGB_R);
				  DelayMs(1000);
				  LedOff(LED_RGB_R);}
			else {
				 LedOn(LED_RGB_G);
				 DelayMs(1000);
			     LedOff(LED_RGB_G);}
	 // No hay nadie con el dedo, sigo esperando.

	 }
	else {
	   	ILI9341DrawFilledRectangle(0, 0, 320, 80, ILI9341_WHITE);
		 UartSendString(SERIAL_PORT_PC, "Esperando...");
		 UartSendString(SERIAL_PORT_PC, "\n\r");
		 ILI9341DrawString(MARGIN, 60 , "ESPERANDO", &font_11x18, ILI9341_BLACK, ILI9341_WHITE);
	     }

	  DelayMs(1000);
	}
    }
return 0;
}
/*==================[end of file]============================================*/

