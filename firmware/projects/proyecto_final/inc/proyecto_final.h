/* Copyright 2018,
 * Juan Manuel Reta
 * jmreta@ingenieria.uner.edu.ar
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
 /*! @mainpage Proyecto_final
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 *Este programa es una aplicacion de un puesto sanitario, en donde al haber una persona se le realiza la medicion de la temperatura
 * de la frecuencia cardiaca, y la oximetria.
 *Estos datos son mandados por puerto serie y por una pantalla.
 * Si el usario presenta una temperatura mayor a 37ºC se enciende el led rojo, si es menor se enciende un led verde.
 * Este programa se enciende y se apaga por la tecla 2
 * Muestra por pantalla una grafica de la oximetria
 *
 * \section hardConn Hardware Connection
 *
 * |    MPC9700		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC 	 	| 	CH1		    |
 * | 	+3,3V 	 	| 	+3,3V		|
 * | 	GND 	 	| 	GND			|
 *
 * |   MA30102		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	IRD 	 	| 	-   		|
 * | 	INT     	| 	-   		|
 * | 	SDA	 	    | 	I2C_SDA     |
 * | 	SCL     	| 	I2C_SCL	    |
 * | 	VIN 	 	| 	+3.3V		|
 * | 	GND 	 	| 	GND			|
 *
 * |   	ILI9341	    |   EDU-CIAA    |
 * |:--------------:|:--------------|
 * | 	SDO/MISO 	|	SPI_MISO	|
 * | 	LED		 	| 	3V3			|
 * | 	SCK		 	| 	SPI_SCK		|
 * | 	SDI/MOSI 	| 	SPI_MOSI	|
 * | 	DC/RS	 	| 	GPIO2		|
 * | 	RESET	 	| 	GPIO3		|
 * | 	CS		 	| 	GPIO0		|
 * | 	GND		 	| 	GND			|
 * | 	VCC		 	| 	3V3			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 6/10 /2021 | creation del programa	                         |
 * | 11/11 /2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Katherine Tschopp \
 *
 */

#ifndef PROYECTO_FINAL_H
#define PROYECTO_FINAL_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef PROYECTO_FINAL_H */

