Puesto sanitario
 Este programa es una aplicacion de un puesto sanitario, en donde al haber una persona se le realiza la medicion de la temperatura
 de la frecuencia cardiaca, y la oximetria.
 Estos datos son mandados por puerto serie y por una pantalla.
 Si el usario presenta una temperatura mayor a 37ºC se enciende el led rojo, si es menor se enciende un led verde.
 Este programa se enciende y se apaga por la tecla 2
 Muestra por pantalla una grafica de la oximetria
 *
 * \section  Hardware Connection
 *
 * |    MPC9700		|   EDU-CIAA	   |
 * |:------------------:|:-----------------|
 * | 	VCC 	 	| 	CH1        |
 * | 	+3,3V 	 	| 	+3,3V	   |
 * | 	GND 	 	| 	GND	   |
 *
 * |   MA30102		|   EDU-CIAA   	    |
 * |:------------------:|:------------------|
 * | 	IRD 	 	| 	-    	    |
 * | 	INT     	| 	-      	    |
 * | 	SDA	        | 	I2C_SDA     |
 * | 	SCL     	| 	I2C_SCL	    |
 * | 	VIN 	 	| 	+3.3V	    |
 * | 	GND 	 	| 	GND         |
 *
 * |   	ILI9341	        |   EDU-CIAA                |
 * |:------------------:|:--------------------------|
 * | 	SDO/MISO 	|	SPI_MISO	|
 * | 	LED      	| 	3V3		|
 * | 	SCK		| 	SPI_SCK		|
 * | 	SDI/MOSI 	| 	SPI_MOSI	|
 * | 	DC/RS	 	| 	GPIO2		|
 * | 	RESET	 	| 	GPIO3		|
 * | 	CS		| 	GPIO0		|
 * | 	GND		| 	GND		|
 * | 	VCC     	| 	3V3		|
