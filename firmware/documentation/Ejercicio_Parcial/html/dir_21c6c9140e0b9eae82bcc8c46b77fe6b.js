var dir_21c6c9140e0b9eae82bcc8c46b77fe6b =
[
    [ "delay.c", "delay_8c_source.html", null ],
    [ "DisplayITS_E0803.c", "_display_i_t_s___e0803_8c_source.html", null ],
    [ "fonts.c", "fonts_8c.html", "fonts_8c" ],
    [ "goniometro.c", "goniometro_8c_source.html", null ],
    [ "hc_sr4.c", "hc__sr4_8c_source.html", null ],
    [ "heartRate.c", "heart_rate_8c_source.html", null ],
    [ "ili9341.c", "ili9341_8c.html", "ili9341_8c" ],
    [ "led.c", "led_8c_source.html", null ],
    [ "max3010x.c", "max3010x_8c_source.html", null ],
    [ "MCP9700.c", "_m_c_p9700_8c_source.html", null ],
    [ "spi.c", "spi_8c.html", "spi_8c" ],
    [ "spo2_algorithm.c", "spo2__algorithm_8c_source.html", null ],
    [ "switch.c", "switch_8c_source.html", null ],
    [ "Tcrt5000.c", "_tcrt5000_8c_source.html", null ]
];