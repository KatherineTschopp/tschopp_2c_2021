var searchData=
[
  ['bare_20metal_20example_20header_20file',['Bare Metal example header file',['../group___baremetal.html',1,'']]],
  ['baud_5frate',['baud_rate',['../structserial__config.html#ae28e9ec22e61f2fd869555513c0857c0',1,'serial_config']]],
  ['bitrate',['bitrate',['../structspi_config__t.html#a9f84d708b15f0c2f9fdd97916673e25d',1,'spiConfig_t']]],
  ['blank_5fporch_5fctrl',['BLANK_PORCH_CTRL',['../ili9341_8c.html#a3864c48a21a720ca2c2e3ea4cd904569',1,'ili9341.c']]],
  ['bool',['bool',['../group___bool.html#gabb452686968e48b67397da5f97445f5b',1,'bool():&#160;bool.h'],['../group___bool.html#gabb452686968e48b67397da5f97445f5b',1,'bool():&#160;bool.h'],['../group___bool.html',1,'(Global Namespace)']]]
];
