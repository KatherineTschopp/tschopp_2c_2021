var searchData=
[
  ['tcrt5000init',['Tcrt5000Init',['../group___l_e_d.html#gab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;Tcrt5000.c'],['../group___l_e_d.html#gab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;Tcrt5000.c']]],
  ['timer',['timer',['../structtimer__config.html#ab42a22a518af439f16e4d09e51aa2553',1,'timer_config']]],
  ['timer_5fconfig',['timer_config',['../structtimer__config.html',1,'']]],
  ['timerinit',['TimerInit',['../group___baremetal.html#ga148b01475111265d1798f5c204a93df0',1,'timer.c']]],
  ['timerreset',['TimerReset',['../group___baremetal.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'timer.c']]],
  ['timerstart',['TimerStart',['../group___baremetal.html#ga31487bffd934ce838a72f095f9231b24',1,'timer.c']]],
  ['timerstop',['TimerStop',['../group___baremetal.html#gab652b899be3054eae4649a9063ec904b',1,'timer.c']]],
  ['transfer_5fmode',['transfer_mode',['../structspi_config__t.html#aef147d342aef67c4adf42dfa2c3a28c5',1,'spiConfig_t']]],
  ['transfermode_5ft',['transferMode_t',['../group___s_p_i.html#ga8eca2297218426636952c631c9a8c881',1,'spi.h']]],
  ['true',['true',['../group___bool.html#ga41f9c5fb8b08eb5dc3edce4dcb37fee7',1,'true():&#160;bool.h'],['../group___bool.html#ga41f9c5fb8b08eb5dc3edce4dcb37fee7',1,'true():&#160;bool.h']]]
];
