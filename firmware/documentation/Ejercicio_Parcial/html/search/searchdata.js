var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuvw",
  1: "adflorst",
  2: "fgis",
  3: "adfghilmstuw",
  4: "bcdefghilmnopstvw",
  5: "cgilst",
  6: "gims",
  7: "bcdefghlmnprsuvw",
  8: "abcdghilmsu",
  9: "e"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

