var searchData=
[
  ['realtimeplot_2eh',['realtimeplot.h',['../realtimeplot_8h.html',1,'']]],
  ['record',['Record',['../struct_record.html',1,'']]],
  ['reset',['RESET',['../ili9341_8c.html#ab702106cf3b3e96750b6845ded4e0299',1,'ili9341.c']]],
  ['rgb_5finterface',['RGB_INTERFACE',['../ili9341_8c.html#a798785f739c085efedff5ff9668f70c2',1,'ili9341.c']]],
  ['right',['RIGHT',['../ili9341_8c.html#a80fb826a684cf3f0d306b22aa100ddac',1,'ili9341.c']]],
  ['rtplotdraw',['RTPlotDraw',['../realtimeplot_8h.html#a2f03f2029ddaefca3b0b1526192d6914',1,'realtimeplot.c']]],
  ['rtplotinit',['RTPlotInit',['../realtimeplot_8h.html#af7abadd5aac29e85950c6621636806d6',1,'realtimeplot.c']]],
  ['rtsignalinit',['RTSignalInit',['../realtimeplot_8h.html#a1ce2cc6a579e86780693670650003ef0',1,'realtimeplot.c']]]
];
