var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuvwxy",
  1: "acdfloprst",
  2: "fgirs",
  3: "adfghilmrstuw",
  4: "bcdefghilmnopstvwxy",
  5: "cgilst",
  6: "gims",
  7: "bcdefghlmnprsuvw",
  8: "abcdghilmsu",
  9: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

