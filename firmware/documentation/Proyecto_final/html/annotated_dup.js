var annotated_dup =
[
    [ "AllPrivate", "struct_all_private.html", "struct_all_private" ],
    [ "analog_input_config", "structanalog__input__config.html", "structanalog__input__config" ],
    [ "creal32_T", "structcreal32___t.html", "structcreal32___t" ],
    [ "creal64_T", "structcreal64___t.html", "structcreal64___t" ],
    [ "creal_T", "structcreal___t.html", "structcreal___t" ],
    [ "digitalIO", "structdigital_i_o.html", "structdigital_i_o" ],
    [ "Font_t", "struct_font__t.html", "struct_font__t" ],
    [ "lcd_cmd_t", "structlcd__cmd__t.html", "structlcd__cmd__t" ],
    [ "orientation_properties_t", "structorientation__properties__t.html", "structorientation__properties__t" ],
    [ "plot_t", "structplot__t.html", "structplot__t" ],
    [ "Record", "struct_record.html", "struct_record" ],
    [ "serial_config", "structserial__config.html", "structserial__config" ],
    [ "signal_t", "structsignal__t.html", "structsignal__t" ],
    [ "spiConfig_t", "structspi_config__t.html", "structspi_config__t" ],
    [ "timer_config", "structtimer__config.html", "structtimer__config" ]
];