/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef MPC9700_H
#define MPC9700_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup MCP9700
 ** @{ */


  /* @brief  EDU-CIAA NXP GPIO driver
  * @author Katherine Tschopp
  *
  * This driver provide functions to medir temperatura por entrada analogica
  *
  *
  * @note
  ** @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 *
 */
/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 *  EF			Eduardo Filomena
 *  JMR			Juan Manuel Reta
 *  SM			Sebastian Mateos
 *  KT          Katherine Tschopp
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include "gpio.h"
#include <stdint.h>

/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2
#define CH0 1  /*Board connector: DAC pin*/
#define CH1 2  /*Board connector: CH1 pin*/
#define CH2 4  /*Board connector: CH2 pin*/
#define CH3 8  /*Board connector: CH3 pin*/
/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/


/** \brief Definition of constants to control the EDU-CIAA .
 **
 **/


/*==================[external functions declaration]=========================*/

/** @fn bool bool MPC9700Init(uint8_t channel)
 * @brief Initialization function of EDU-CIAA
 * Mapping ports (PinMux function), set direction and initial state of ports
 * @param[in] Edu cia nxp
 * @return TRUE if no error
 */
bool MPC9700Init(uint8_t channel);


/** @fn  float MPC9700ReadTemp(void)
 * @brief Function devuelve la temperatura del sensor de temperatura en grados celsius
 * @param[in] No parameter
 * @return Temperatura en celsius
 */
 float MPC9700ReadTemp(void);

/*==================[end of file]============================================*/
#endif /* #ifndef MPC9700_H */

