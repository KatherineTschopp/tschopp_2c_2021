/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef HC_SR4_H
#define HC_SR4_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup HC_SR4
 ** @{ */


  /* @brief  EDU-CIAA NXP GPIO driver
  * @author Katherine Tschopp
  *
  * This driver provide functions to medir distancias por ultrasonido hc_sr4
  *
  *
  * @note
  ** @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 *
 */
/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 *  EF			Eduardo Filomena
 *  JMR			Juan Manuel Reta
 *  SM			Sebastian Mateos
 *  KT          Katherine Tschopp
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include "gpio.h"
#include <stdint.h>

/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/



/** \brief Definition of constants to control the EDU-CIAA .
 **
 **/


/*==================[external functions declaration]=========================*/

/** @fn bool HcSr04Init(gpio_t echo, gpio_t trigger)
 * @brief Initialization function of EDU-CIAA
 * Mapping ports (PinMux function), set direction and initial state of ports
 * @param[in] Edu cia nxp
 * @return TRUE if no error
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);


/** @fn  int16_t HcSr04ReadDistanceCentimeters(void)
 * @brief Function devuelve la distancia del ultrasonido en centimetros
 * @param[in] No parameter
 * @return Distancia en centrimetros
 */
 int16_t HcSr04ReadDistanceCentimeters(void);

/** @fn int16_t HcSr04ReadDistanceInches(void)
 * @brief Function devuelve la distancia del ultrasonido en pulgadas
 * @param[in] No parameter
 * @return Distancia en pulgadas
 */
 int16_t HcSr04ReadDistanceInches(void);

/** @fn bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
 * @brief Function desinicialiaz function of EDU-CIAA
 * @param[in] param[in] Edu cia nxp
 * @return FALSE if an error occurs, in other case returns TRUE
 */
 bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef HC_SR4_H */

