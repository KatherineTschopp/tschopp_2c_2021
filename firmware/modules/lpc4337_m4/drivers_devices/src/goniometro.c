	/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup GONIOMETRO
 ** @{ */


  /* @brief  EDU-CIAA NXP GPIO driver
  * @author Katherine Tschopp
  *
  * This driver provide functions para determinar angulos por medio de un potenciometro.
  *
  *
  * @note
  ** @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Driver creation		                         |
 *
 */
/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 * EF		Eduardo Filomena
 * JMR		Juan Manuel Reta
 * SM		Sebastian Mateos
 * KT       Katherine Tschopp
 */



/*==================[inclusions]=============================================*/
#include "goniometro.h"
#include "analog_io.h"



/*==================[macros and definitions]=================================*/
uint8_t channel_global;
#define PENDIENTE	83.33 /*(75/0.9)*/
#define ORDENADA	100 /*(75/0.9)*1.2*/
#define PI	3.14
#define CONVERSION	180
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
uint16_t value;
/*==================[external data definition]===============================*/
analog_input_config analogico; /* Defino la estructura del analogico para inicializar */
/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
bool GoniometroInit(uint8_t channel){

	/** Defin variables globales*/
    channel_glob =channel;
	/** Defin estructura de analogico*/
	analogico.input =channel;
    analogico.mode =AINPUTS_SINGLE_READ;
	 //   analogico.pAnalogInput= &NULL ;
	 /** incializo analogico*/
	 AnalogInputInit(&analogico);

		return true;
}


/** \brief Function to turn on a specific goniometro */
int16_t GonimetroReadAngle(void) {

	int16_t angle;
	AnalogInputReadPolling(channel_glob, &value);
	angulo=  (PENDIENTE * value) - ORDENADA;
	return (angle);
}
int16_t GonimetroReadRad(void) {

	int16_t angle;
	int16_t rad;
	AnalogInputReadPolling(channel_glob, &value);
	angulo=  (PENDIENTE * value) - ORDENADA;
	rad= angulo* (PI/CONVERSION);
	return (rad);
}


bool GoniometroDeinit(){

	return 0;
}


/*==================[end of file]============================================*/
