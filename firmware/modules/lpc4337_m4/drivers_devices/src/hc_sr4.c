	/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup HC_SR4
 ** @{ */


  /* @brief  EDU-CIAA NXP GPIO driver
  * @author Katherine Tschopp
  *
  * This driver provide functions to medir distancias por ultrasonido hc_sr4
  *
  *
  * @note
  ** @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 *
 */
/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 * EF		Eduardo Filomena
 * JMR		Juan Manuel Reta
 * SM		Sebastian Mateos
 * KT       Katherine Tschopp
 */



/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/
gpio_t echo_global;
gpio_t trigger_global;
#define CONSTANT_CENTIMETERS 30
#define CONSTANT_INCHES 148

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
bool HcSr04Init(gpio_t echo, gpio_t trigger){

	/** Configuration of the GPIO */
	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);

	/** Turn off leds*/
	GPIOOff(trigger);

	/** Defin variables globales*/
	echo_global=echo;
	trigger_global=trigger;
	return true;
}


/** \brief Function to turn on a specific ultrasound */
int16_t HcSr04ReadDistanceCentimeters(void) {

	int16_t counter=0;
	int16_t distance_centimeters;

	GPIOOn(trigger_global);
	DelayUs (10);
	GPIOOff(trigger_global);

	while (! GPIORead(echo_global))
	{ }
	while (GPIORead(echo_global))
    { counter +=1 ;
      DelayUs (1);
    }
	distance_centimeters= counter/CONSTANT_CENTIMETERS ;
	return (distance_centimeters);
}
int16_t HcSr04ReadDistanceInches(void) {
    int16_t counter=0;
	int16_t distance_inches;

	GPIOOn(trigger_global);
	DelayUs (10);
	GPIOOff(trigger_global);

	while (! GPIORead(echo_global))
	{ }
	while (GPIORead(echo_global))
    { counter +=1 ;
      DelayUs (1);
    }
	distance_inches=counter/ CONSTANT_INCHES  ;
	return (distance_inches);
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger){
	GPIOInit(echo,trigger);
	return 0;
}


/*==================[end of file]============================================*/
