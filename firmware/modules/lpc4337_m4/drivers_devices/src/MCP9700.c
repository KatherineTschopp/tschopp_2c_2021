	/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup MCP9700
 ** @{ */


  /* @brief  EDU-CIAA NXP GPIO driver
  * @author Katherine Tschopp
  *
  * This driver provide functions to medir temperatura por medio de entrada analogica
  *
  *
  * @note
  ** @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 *
 */
/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 * EF		Eduardo Filomena
 * JMR		Juan Manuel Reta
 * SM		Sebastian Mateos
 * KT       Katherine Tschopp
 */



/*==================[inclusions]=============================================*/
#include "MPC9700.h"
#include "gpio.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/
float value_tension;
float constant =0.00322581;
float temp;
uint16_t value;
uint8_t channel_glob;
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/
analog_input_config analogico;
/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
bool MPC9700Init(uint8_t channel){
	/** Defin variables globales*/
	channel_glob =channel;
	/** Defin estructura de analogico*/
    analogico.input =channel;
    analogico.mode =AINPUTS_SINGLE_READ;
 //   analogico.pAnalogInput= &NULL ;
      /** incializo analogico*/
    AnalogInputInit(&analogico);

	return true;
}


/** \brief Function to turn on a specific SENSOR TEMP */
float MPC9700ReadTemp(void) {
	AnalogInputReadPolling(channel_glob, &value);
    value_tension = (value*constant);
    temp=(value_tension-0.5)/0.01;
	return (temp);
}




/*==================[end of file]============================================*/
