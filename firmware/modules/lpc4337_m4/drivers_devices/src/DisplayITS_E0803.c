/* Copyright 2016,
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for DisplayITS_E0803 in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 * EF		Eduardo Filomena
 * JMR		Juan Manuel Reta
 * SM		Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version Leando Medus
 * 20160807 v0.2 modifications and improvements made by Eduardo Filomena
 * 20160808 v0.3 modifications and improvements made by Juan Manuel Reta
 * 20180210 v0.4 modifications and improvements made by Sebastian Mateos
 * 20190820 v1.1 new version made by Sebastian Mateos
 */

/*==================[inclusions]=============================================*/
#include "DisplayITS_E0803.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
//se declara la función BinaryToBcd que recibe un entero de 32 bits,  la cantidad de dígitos de salida y un puntero a un arreglo donde se almacenarán los n dígitos
void BCD_to_port(uint8_t bcd_digit);
//se declara la función BinaryToBcd que recibe un entero de 32 bits,  la cantidad de dígitos de salida y un puntero a un arreglo donde se almacenarán los n dígitos
void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
void BCD_to_port(uint8_t bcd_digit){

	uint8_t mascara=1;
	uint8_t auxiliar=0;
	auxiliar=bcd_digit&mascara;

	if(auxiliar){
		GPIOOn(GPIO_LCD_1);
	}
	else{
		GPIOOff(GPIO_LCD_1);
	}
	mascara=mascara << 1;
	auxiliar=bcd_digit&mascara;
	if(auxiliar){
		GPIOOn(GPIO_LCD_2);
	}
	else{
		GPIOOff(GPIO_LCD_2);
	}
	mascara=mascara << 1;
	auxiliar=bcd_digit&mascara;
	if(auxiliar){
		GPIOOn(GPIO_LCD_3);
	}
	else{
		GPIOOff(GPIO_LCD_3);
	}
	mascara=mascara << 1;
	auxiliar=bcd_digit&mascara;
	if(auxiliar){
		GPIOOn(GPIO_LCD_4);
	}
	else{
		GPIOOff(GPIO_LCD_4);
	}
}



void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number)
{
	uint32_t aux=data;
	while(digits>0){
		digits--;
		bcd_number[digits]=aux%10;
		aux=aux/10;
	}
}

/*==================[external functions definition]==========================*/
bool ITSE0803Init(){

	/** Configuration of the GPIO */
	GPIOInit(GPIO_LCD_1, GPIO_OUTPUT);
	GPIOInit(GPIO_LCD_2, GPIO_OUTPUT);
	GPIOInit(GPIO_LCD_3, GPIO_OUTPUT);
	GPIOInit(GPIO_LCD_4, GPIO_OUTPUT);
	GPIOInit(GPIO_1, GPIO_OUTPUT);
	GPIOInit(GPIO_3, GPIO_OUTPUT);
	GPIOInit(GPIO_5, GPIO_OUTPUT);

	GPIOOff(GPIO_LCD_1);
	GPIOOff(GPIO_LCD_2);
	GPIOOff(GPIO_LCD_3);
	GPIOOff(GPIO_LCD_4);
	GPIOOff(GPIO_1);
	GPIOOff(GPIO_3);
	GPIOOff(GPIO_5);

	return true;
}
bool ITSE0803DisplayValue(uint16_t valor){
	uint8_t bcd_number[3];
	BinaryToBcd (valor, 3, bcd_number);

	BCD_to_port(bcd_number[0]);
	GPIOOn(GPIO_1);
	GPIOOff(GPIO_1);
	BCD_to_port(bcd_number[1]);
	GPIOOn(GPIO_3);
	GPIOOff(GPIO_3);
	BCD_to_port(bcd_number[2]);
	GPIOOn(GPIO_5);
	GPIOOff(GPIO_5);

}

uint16_t ITSE0803ReadValue(void){

}

bool ITSE0803Deinit(gpio_t * pins){

	GPIODeinit();
	return true;
}



/*==================[end of file]============================================*/
