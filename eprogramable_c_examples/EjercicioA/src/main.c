/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/
struct leds
{
	uint8_t n_led;       // indica el número de led a controlar
	uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;   // indica el tiempo de cada ciclo
	uint8_t mode;     //  ON, OFF, TOGGLE
} my_leds;


/*==================[internal functions declaration]=========================*/
void control_led (struct leds *puntero)
{
#define on 1
#define off 2
#define toggle 3

	uint8_t indice=0;
	uint8_t indice_2=0;

switch(puntero->mode)
{
    case on:
    {
				printf("Poner el led %d en ALTO \r\n", puntero->n_led);
				break;

	      } break;
  case off:
  {
	  				printf("Poner el led %d en BAJO \r\n", puntero->n_led);
	  				break;

	  } break;

 case toggle:  {
  for (indice; indice<puntero->n_ciclos; indice++)  {
		            printf("Poner el led %d en ALTO \r\n", puntero->n_led);

                       for (indice_2; indice_2<puntero->periodo; indice_2++)
                       {   printf("Esperando\r\n");

                       } indice_2=0;

	                printf("Poner el led %d en BAJO \r\n", puntero->n_led);
	                for (indice_2; indice_2<puntero->periodo; indice_2++)

	                {
	                	printf("Esperando\r\n");
	                }indice_2=0; }

} break;

} return 0;
}
int main(void)
{

my_leds.mode =3;
my_leds.n_led =1;
my_leds.n_ciclos =3;
my_leds.periodo =2;
control_led(&my_leds);
}

/*==================[end of file]============================================*/

