/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/
	uint32_t variable_32= 0x01020304 ;

	uint8_t variable_1 ;

	uint8_t variable_2 ;

	uint8_t variable_3 ;

	uint8_t variable_4 ;


/*==================[internal functions declaration]=========================*/
	union variables{
		struct{
			uint8_t byte1;
			uint8_t byte2;
			uint8_t byte3;
			uint8_t byte4;
		}cada_byte;
		uint32_t  todos_los_bytes ;
	           } variable_union;
int main(void)
{
	printf("\nVariable: %x",variable_32);

	variable_1 = variable_32 ;

	variable_2 = (variable_32>>8 );

	variable_3 = (variable_32>>16);

	variable_4 = (variable_32>>24);

	printf ("\n%x",variable_1);
	printf ("\n%x",variable_2);
	printf ("\n%x",variable_3);
	printf ("\n%x",variable_4);

//Consigna 16.b

     variable_union.todos_los_bytes = 0x01020304;
	variable_union.cada_byte.byte1 = variable_union.todos_los_bytes>>32;
	printf("\nDatos: %d",variable_union.cada_byte.byte1);
	printf("\nDatos: %d",variable_union.cada_byte.byte2);
	printf("\nDatos: %d",variable_union.cada_byte.byte3);
	printf("\nDatos: %d",variable_union.cada_byte.byte4);


}

/*==================[end of file]============================================*/

