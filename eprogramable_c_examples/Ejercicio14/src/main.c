/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t nombre[12];
	uint8_t apellido[20];
	uint8_t edad;
} Alumno_t;

/*==================[internal functions declaration]=========================*/

int main(void)
{
	Alumno_t mi_persona= {"Katherine","Tschopp",22};

		printf("Mi nombre es %s\r\n",mi_persona.nombre);
		printf("Mi apellido es %s\r\n",mi_persona.apellido);
		printf("Mi edad es %d\r\n",mi_persona.edad);


		Alumno_t *alum;
		Alumno_t companero;
		alum= &companero;

		uint8_t ape1[]= "Querubin";
		uint8_t nom1[]= "Luciano";

		alum->edad = 23;
		strcpy(alum->apellido, ape1);
		strcpy(alum->nombre, nom1);

		printf("El nombre de mi compañero es %s\r\n",companero.nombre);
		printf("El apellido de mi compañero es %s\r\n",companero.apellido);
		printf("su edad es %d\r\n",companero.edad);

}

/*==================[end of file]============================================*/

